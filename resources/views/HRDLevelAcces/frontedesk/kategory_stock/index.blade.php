@extends('layout')

@section('title')
    (hr) Stocked
@stop

@section('top')
    @include('assets_css_1')
    @include('assets_css_2')

@stop

@section('navbar')
    @include('navbar_top')
    @include('navbar_left', [
        'c16' => 'active'
    ])
@stop
@section('body')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Category Stationary</h1> 
    </div>
</div>
<div class="row">
    <div class="col-lg-12 ">
    <a href="{{route('addKategoryStationary')}}" class="btn btn-sm btn-info">Add Kategory</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-2 pull-right">             
             {{ $kategori_stationary->links('') }}
        </div>       
    </div>
    <div class="col-lg-12">
        <table class="table table-hover" id="myTable">
            <thead>
                <tr>
                    <th style="text-align: center">No</th>
                    <th style="text-align: center">Unique Code</th>
                    <th style="text-align: center">Kategory</th>
                    <th style="text-align: center">Action</th>
                </tr>
            </thead>
            <?php $no = 1; ?>
            <tbody>
                <?php foreach ($kategori_stationary as $kk): ?>
                 <tr>
                    <td style="text-align: center">{{$no++}}</td>
                    <td style="text-align: center">{{$kk->unik_kategori}}</td>
                    <td style="text-align: center">{{$kk->kategori_stock}}</td>
                    <td style="text-align: center"><a href="{{route('editKategoryStationary', [$kk->id])}}" class="btn btn-sm btn-default">Edit Name</a></td>
                   <!--  <td><a href="#" class="btn btn-sm btn-default">Delete</a></td> -->
                </tr>   
                <?php endforeach ?>
                
            </tbody>
        </table>
    </div>    
</div>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
@stop

@section('bottom')
    @include('assets_script_1')
    @include('assets_script_2')
    @include('assets_script_7')
@stop