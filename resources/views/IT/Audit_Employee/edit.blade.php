@extends('layout')

@section('title')
    (it) Change Data Employee
@stop

@section('top')
    @include('assets_css_1')
    @include('assets_css_2')
	 @include('assets_css_3')
@stop

@section('navbar')
    @include('navbar_top')
    @include('navbar_left', [
        'c1u' => 'collape in',
        'c1' => 'active', 'c15' => 'active'
    ])
@stop

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Data Employee</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">                
                <div class="panel-heading">

                    <h5 class="panel-title">
                        <b>Form {{$users->first_name." ".$users->last_name}} {{$users->join_date}} {{$users->nik}}</b>
                    </h5>
                </div>               

                <div class="panel-body">
                {!! Form::open(['route' => ['audit/post', $users->id], 'role' => 'form', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) !!}
                    {{ csrf_field() }}
                        <div class="row">

                             <div class="col-lg-2">                              
                                @if ($errors->has('nik_OLD'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('nik_OLD', 'NIK OLD') !!}
                                    {!! Form::text('nik_OLD', $nik_old, ['class' => 'form-control', 'placeholder' => 'nik_OLD', 'readonly']) !!}
                                    <p class="help-block">{!! $errors->first('nik_OLD') !!}</p>
                                </div>
                            </div>                          
                             <div class="col-lg-2">                              
                                @if ($errors->has('username_old'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('username_old', 'Username OLD') !!}
                                    {!! Form::text('username_old', $first_name, ['class' => 'form-control', 'placeholder' => 'username old', 'readonly']) !!}
                                    <p class="help-block">{!! $errors->first('username_old') !!}</p>
                                </div>
                            </div>
                            <div class="col-lg-2">                              
                                @if ($errors->has('username'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('username', 'Username') !!}
                                    {!! Form::text('username',  old('username'), ['class' => 'form-control', 'placeholder' => 'Username', 'required' => true]) !!}
                                    <p class="help-block">{!! $errors->first('username') !!}</p>
                                </div>
                            </div>
                        <div class="col-lg-2">
                                @if ($errors->has('email'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('email', 'Email') !!}
                                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'required' => true]) !!}
                                    <p class="help-block">{!! $errors->first('email') !!}</p>
                                </div>
                            </div>                        
                           <div class="col-lg-4">
                                @if ($errors->has('name'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('name', 'Name Employee') !!}
                                    {!! Form::text('name', $users->first_name." ".$users->last_name, ['class' => 'form-control', 'required' => false, 'readonly' => 'true']) !!}
                                    <p class="help-block">{!! $errors->first('name') !!}</p>
                                </div>
                            </div>
                             <div class="col-lg-2">
                                @if ($errors->has('emp'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('emp', 'Employee Status') !!}
                                    {!! Form::text('emp', $users->emp_status, ['class' => 'form-control', 'required' => false, 'readonly' => 'true']) !!}
                                    <p class="help-block">{!! $errors->first('emp') !!}</p>
                                </div>
                            </div>
                             <div class="col-lg-2">
                                @if ($errors->has('start'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('start', 'Join Date') !!}
                                    {!! Form::text('start', $users->join_date, ['class' => 'form-control', 'required' => false, 'readonly' => 'true']) !!}
                                    <p class="help-block">{!! $errors->first('start') !!}</p>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Save', ['class' => 'btn btn-sm btn-success']) !!}
                        <a class="btn btn-sm btn-warning" href="{{route('index-audit')}}">Back</a>
                        {!! Form::close() !!}
                    </div>
                </div>
                       
					
				
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>         
        </div>
        <div class="modal-body">
          <p>Are you sure want to update data.</p>
        </div>
        <div class="modal-footer">
		{!! Form::submit('Save', ['onclick' => 'myFunction', 'title' => 'Save', 'class' => 'btn btn-sm btn-success', 'data-toggle' => 'modal', 'data-target' => '#Save'])!!}
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
					    
                    {!! Form::close() !!}
					
                </div>
            </div>
        </div>
    </div>
	
</div>
	
@stop

@section('bottom')
    @include('assets_script_1')
    @include('assets_script_2')
@stop

