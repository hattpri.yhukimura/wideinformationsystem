@extends('layout')

@section('title')
    Leave
@stop

@section('top')
    @include('assets_css_1')
    @include('assets_css_2')
@stop

@section('navbar')
    @include('navbar_top')
    @include('navbar_left', [
        'c2' => 'active'
    ])
@stop
@section('body')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Leave Approval</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">	
     <!--  <div><h4><b>Aprroval Project <?php $f = DB::table('users')->where('users.id', '=', Auth::user()->id)->join('project_category', 'users.project_category_id_1', '=', 'project_category.id')->value('project_category.project_name'); echo $f;?></b></h4></div>	 -->        

			<table class="table table-striped table-hover" width="100%" id="tables">
				<thead>
				    <tr>
                        <td>ID</td>
                        <td>Leave Date</td>
                        <td>NIK</td>
                        <td>Name</td>
                        <td>Leave Category</td>
                        <td>Department</td>
                        <td>Total Day</td>                         
                        <td>Supervisor<br>Approval</td>
                         <td>Coordinator<br>Approval</td> 
                        <td style="width: 78px;">Approval</td>
				    </tr>
				</thead>
			</table>      
            </div>
        <!-- <?php if (auth::user()->project_category_id_2 != null) {
       ?>     
            <div><h4><b>Aprroval Project <?php $f = DB::table('users')->where('users.id', '=', Auth::user()->id)->join('project_category', 'users.project_category_id_2', '=', 'project_category.id')->value('project_category.project_name'); echo $f;?></b></h4></div>
            <table class="table table-striped table-hover" width="100%" id="tables2">
                <thead>
                    <tr>
                         <td>ID</td>
                        <td>Leave Date</td>
                        <td>NIK</td>
                        <td>Name</td>
                        <td>Leave Category</td>
                        <td>Department</td>
                        <td>Total Day</td>                         
                        <td>Supervisor<br>Approval</td>
                         <td>Coordinator<br>Approval</td> 
                        <td style="width: 78px;">Approval</td>
                    </tr>
                </thead>
            </table>
                
         <?php } if (auth::user()->project_category_id_3 != null) {
       ?>      
            <div><h4><b>Aprroval Project <?php $f = DB::table('users')->where('users.id', '=', Auth::user()->id)->join('project_category', 'users.project_category_id_3', '=', 'project_category.id')->value('project_category.project_name'); echo $f;?></b></h4></div>
            <table class="table table-striped table-hover" width="100%" id="tables3">
                <thead>
                    <tr>
                       <td>ID</td>
                        <td>Leave Date</td>
                        <td>NIK</td>
                        <td>Name</td>
                        <td>Leave Category</td>
                        <td>Department</td>
                        <td>Total Day</td>                         
                        <td>Supervisor<br>Approval</td>
                         <td>Coordinator<br>Approval</td> 
                        <td style="width: 78px;">Approval</td>
                    </tr>
                </thead>
            </table>           
        <?php }  ?>
        </div>

    </div> -->

    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content">
                <!--  -->
            </div>
        </div>
    </div>
@stop

@section('bottom')
    @include('assets_script_1')
    @include('assets_script_2')
@stop

@section('script')
	$('[data-toggle="tooltip"]').tooltip();

    $('#tables').DataTable({
        "columnDefs": [
            { className: "never", "searchable": false, "orderable": false, "visible": false, "targets": [0] }
        ],
    	"order": [
    		[ 0,"des" ]
    	],
        responsive: true,        
        ajax: '{!! URL::route("Supervisor/getindexSPV_Approval") !!}'
    });

     $('#tables2').DataTable({
        "columnDefs": [
            { className: "never", "searchable": false, "orderable": false, "visible": false, "targets": [0] }
        ],
        "order": [
            [ 0,"des" ]
        ],
        responsive: true,        
        ajax: '{!! URL::route("Supervisor/getindexSPV_Approval2") !!}'
    });

     $('#tables3').DataTable({
        "columnDefs": [
            { className: "never", "searchable": false, "orderable": false, "visible": false, "targets": [0] }
        ],
        "order": [
            [ 0,"des" ]
        ],
        responsive: true,        
        ajax: '{!! URL::route("Supervisor/getindexSPV_Approval3") !!}'
    });

    $(document).on('click','#tables tr td a[title="Detail"]',function(e) {
        var id = $(this).attr('data-role');

        $.ajax({
            url: id, 
            success: function(e) {
                $("#modal-content").html(e);
            }
        });
    });

    $(document).on('click','#tables2 tr td a[title="Detail"]',function(e) {
        var id = $(this).attr('data-role');

        $.ajax({
            url: id, 
            success: function(e) {
                $("#modal-content").html(e);
            }
        });
    });

    $(document).on('click','#tables3 tr td a[title="Detail"]',function(e) {
        var id = $(this).attr('data-role');

        $.ajax({
            url: id, 
            success: function(e) {
                $("#modal-content").html(e);
            }
        });
    });
@stop