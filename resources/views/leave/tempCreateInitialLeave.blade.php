@extends('layout')

@section('title')
    (hr) Add Temporary Initial Leave
@stop

@section('top')
    @include('assets_css_1')
@stop

@section('navbar')
    @include('navbar_top')
    @include('navbar_left', [
        'c17' => 'active'
    ])
@stop

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Initial Leave Transaction</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <b>Form Initial Leave Transaction</b>
                    </h5>
                </div>

                <div class="panel-body">
                    {!! Form::open(['route' => ['hr_mgmt-data/leave/tempStoreInitialLeave', $users->id], 'role' => 'form', 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-lg-2">
                                @if ($errors->has('NIK'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('nik', 'NIK') !!}
                					{!! Form::text('nik', $users->nik, ['class' => 'form-control', 'placeholder' => 'NIK', 'maxlength' => 20, 'required' => true, 'readonly' => true]) !!}
                                    <p class="help-block">{!! $errors->first('nik') !!}</p>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                @if ($errors->has('first_name'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('first_name', 'First Name') !!}
                                    {!! Form::text('first_name', $users->first_name, ['class' => 'form-control', 'placeholder' => 'First Name', 'maxlength' => 20, 'required' => true, 'readonly' => true]) !!}
                                    <p class="help-block">{!! $errors->first('first_name') !!}</p>
                            </div>
                        </div>

                            <div class="col-lg-2">
                                @if ($errors->has('last_name'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('last_name', 'Last Name') !!}
                					{!! Form::text('last_name', $users->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name', 'maxlength' => 20, 'required' => true, 'readonly' => true]) !!}
                                    <p class="help-block">{!! $errors->first('last_name') !!}</p>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                @if ($errors->has('department'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('department', 'Department') !!}
                                    {!! Form::text('department', $department, ['class' => 'form-control', 'placeholder' => 'Department', 'maxlength' => 20, 'required' => true, 'readonly' => true]) !!}
                                    <p class="help-block">{!! $errors->first('department') !!}</p>
                                </div>
                            </div>                          
                        </div>

                       <div class="row">
                            <div class="col-lg-2">
                                @if ($errors->has('leave_category'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('leave_category', 'Leave Category') !!}<font color="red"> (*)</font>
                                    {!! Form::select('leave_category', $leave_category, old('leave_category'), ['class' => 'form-control', 'maxlength' => 5, 'required' => true]) !!}
                                    <p class="help-block">{!! $errors->first('leave_category') !!}</p>
                                </div>
                            </div>                        
                            <div class="col-lg-2">
                                 @if ($errors->has('initial'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('initial', 'Remains of Annual Leave') !!}<!-- <font color="red"> (*)</font> -->
                                    {!! Form::text('initial', $all_leave_entitled, ['class' => 'form-control', 'placeholder' => 'Last Name', 'maxlength' => 20, 'required' => true, 'readonly' => true, 'id' => 'initial']) !!}
                                    <p class="help-block">{!! $errors->first('initial') !!}</p>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                 @if ($errors->has('initial'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('initial_exdo', 'Remains of Exdo Leave') !!}<!-- <font color="red"> (*)</font> -->
                                    {!! Form::text('initial_exdo', $all_exdo_entitled, ['class' => 'form-control', 'placeholder' => 'Last Name', 'maxlength' => 20, 'required' => true, 'readonly' => true, 'id' => 'initial_exdo']) !!}
                                    <p class="help-block">{!! $errors->first('initial') !!}</p>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                @if ($errors->has('initial_transaction'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('initial_transaction', 'Initial Transaction') !!}
                                    {!! Form::text('initial_transaction', old('initial_transaction'), ['class' => 'form-control', 'placeholder' => 'Number', 'maxlength' => 20, 'id' => 'initial_transaction']) !!}
                                    <p class="help-block">{!! $errors->first('initial_transaction') !!}</p>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                @if ($errors->has('leave_date'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('leave_date', 'Start Leave Date') !!}<br>
                                    {!! Form::date('leave_date', null, ['class' => 'form-control', 'required' => true]) !!}
                                    <p class="help-block">{!! $errors->first('leave_date') !!}</p>
                                </div>
                            </div>

                            <div class="col-lg-2"id="tgl2">
                                @if ($errors->has('end_leave_date'))
                                    <div class="form-group has-error">
                                @else
                                    <div class="form-group">
                                @endif
                                    {!! Form::label('end_leave_date', 'End Leave Date') !!}<br>
                                    {!! Form::date('end_leave_date', null, ['class' => 'form-control', 'required' => true]) !!}
                                    <p class="help-block">{!! $errors->first('end_leave_date') !!}</p>
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-lg-2">
                                    <div class="form-group">
                                    {!! Form::label('reason', 'Reason') !!}
                                    {!! Form::textarea('reason', old('reason'), ['class' => 'form-control', 'placeholder' => 'max 100 word', 'maxlength' => 100, 'autosave' => true]) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::submit('Add', ['class' => 'btn btn-sm btn-success']) !!}
                        <a class="btn btn-sm btn-warning" href="{!! URL::route('hr_mgmt-data/leave/tempInitialLeave') !!}">Back</a>
                        {!! Form::close() !!}
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
<script src="{!! URL::route('assets/js/jquery') !!}"></script>
 <script>
function hitung2() {
    var a = $("#initial").val();
    var b = $("#initial_transaction").val();
    c = a - b; //a min b
    $("#initial").val(c);

}
</script>

@stop

@section('bottom')
    @include('assets_script_1')
@stop