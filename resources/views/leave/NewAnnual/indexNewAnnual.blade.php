@extends('layout')

@section('title')
    Leave
@stop

@section('top')
    @include('assets_css_1')
    @include('assets_css_2')
@stop

@section('navbar')
    @include('navbar_top')
    @include('navbar_left', [
        'c2' => 'active'
    ])
@stop
@section('body')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <div class="row">
        <div class="col-lg-10">
            <h1 class="page-header">Applying Leave</h1>           
        </div>
        <div class="col-lg-2">
            @if(auth::user()->id === 226)
           <a class="btn btn-sm page-header" style="color: black;" title="Leave Guide" href="https://drive.google.com/file/d/1e3kDgrBQYIYqwMhhz5YwGqaZF9Hc2daN/view?usp=sharing" target="_blank"> <i class="material-icons">&#xe63a;</i></a>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">                           
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed" id="tables">
                        <thead>
                            <tr style="white-space:nowrap">
                                <th>Leave Category</th>
                                <th>Available</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                         <tbody>
                            <tr style="white-space:nowrap">
                                <td>Annual</td>
                                <td>
                                    <?php if (auth::user()->emp_status === "Permanent"): ?>
                                        {{ $totalAnnualPermanent1 }}
                                    <?php else: ?>
                                        {{ $totalAnnual }}
                                    <?php endif ?>
                                </td>
                                <td>
                                

                                        <?php if (auth::user()->emp_status === "Permanent"): ?>
                                            @if($totalAnnualPermanent1 > 0) 
                                            <a href="{!! URL::route('leave/create') !!}" class="btn btn-primary btn-xs" role="button">Apply</a>
                                            @endif
                                        <?php else: ?>
                                             @if($totalAnnual > 0) 
                                            <a href="{!! URL::route('leave/create') !!}" class="btn btn-primary btn-xs" role="button">Apply</a>
                                            @endif
                                        <?php endif ?>                                
                                </td>
                                <td></td>
                            </tr>
                            <tr style="white-space:nowrap">
                                <td>Exdo</td>
                                <td>{{ $remainExdo }}</td>
                                <td>
                                       @if($remainExdo > 0 )
                                   
                                        <a href="{!! URL::route('leave/createExdo') !!}" class="btn btn-primary btn-xs" role="button">Apply</a>
                                  
                                         @endif
                                </td>
                                <td></td>
                            </tr>
                            <tr style="white-space:nowrap">
                                <td>Etc</td>
                                <td>-</td>
                                <td>
                                    <a href="{!! URL::route('leave/createEtc') !!}" class="btn btn-primary btn-xs" role="button">Apply</a></td>
                                   
                                <td></td>
                            </tr>
                            <tr style="white-space:nowrap">
                                <td>Advance Leave</td>
                                <td>{{ $user->initial_annual - $annual->transactionAnnual }}</td>
                                <td>
                                    @if($user->initial_annual - $annual->transactionAnnual > 0)
                                    <a href="{!! URL::route('createAdvanceLeave') !!}" class="btn btn-primary btn-xs" role="button">Apply</a>
                                    @endif
                                </td>

                                <td></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div> 
            <p style="font-size: 11px;"><i>Note : Please check the rest of your leave, if there are differences please contact the receptionist </i></p>
        </div>
        <div class="col-lg-8">
            <h4>Index Annual</h4>
            <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed" id="indexAnnual">
                <thead>
                    <tr style="white-space:nowrap">
                       <th>Year</th>
                       <th>Balance</th>
                       <th>Taken</th>
                       <th>Remains</th>
                       <th>Available</th>
                    </tr>                    
                </thead>
                <tbody>
                    <tr>
                        <td>{{ date('Y') }}</td>
                        <td>{{ $user->initial_annual }}</td>
                        <td>{{ $annual->transactionAnnual }}</td>
                        <td>{{ $user->initial_annual - $annual->transactionAnnual }}</td>
                        <td>
                            <?php if (auth::user()->emp_status === "Permanent"): ?>
                                 {{ $totalAnnualPermanent1 }}
                            <?php else: ?>
                                 {{ $totalAnnual }}
                            <?php endif ?>
                        </td>
                    </tr>      
                </tbody>              
            </table>
        </div>
        </div>
    </div>
<div class="row">       
        <div class="col-lg-12">
            <h4 class="page-header">Index Exdo</h4>
        </div>
    </div>
<div class="row">   
    <div class="col-lg-6">      
         <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed" id="indexExdo">
                <thead>
                    <tr style="white-space:nowrap">
                       <th>No</th>
                       <th>Expired</th>
                       <th>Initial</th>
                       <th>Limit</th>  
                       <th>Note</th>
                    </tr>                    
                </thead>               
            </table>
        </div>
    </div>
    <!-- <hr> -->
    <div class="col-lg-4">
         <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed" id="indexstatExdo">
                <thead>
                    <tr style="white-space:nowrap">
                       <th>Total Initial</th>
                       <th>Taken</th>
                       <th>Remains</th>  
                       <th>Available</th> 
                    </tr>                    
                </thead> 
                <tbody>
                    <tr>
                        <td>{{ $exdo->sum() }}</td>
                        <td> {{ $minusExdo->sum() }} </td>
                        <td>{{ $remainExdo }}</td>
                        <td>{{ $remainExdo }}</td>
                    </tr>
                </tbody>              
            </table>
        </div>
    </div>
</div>
@stop

@section('bottom')
    @include('assets_script_1')
    @include('assets_script_2')
@stop

@section('script')
    $('[data-toggle="tooltip"]').tooltip();
   
       $('#indexExdo').DataTable({
                    processing: true,
                    ajax: '{{ route('indexDataExdo') }}',
                    columns: [
                        { data: 'DT_Row_Index', orderable: false, searchable : false}, 
                        { data: 'expired'},
                        { data: 'initial'},
                        { data: 'difforHumans'},
                        { data: 'note'}
                          
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                         'excel'
                    ]                 
            });
   



    $(document).on('click','#indexExdo tr td a[title="Detail"]',function(e) {
        var id = $(this).attr('data-role');

        $.ajax({
            url: id, 
            success: function(e) {
                $("#modal-content").html(e);
            }
        });
    });
@stop