<?php

namespace App\Http\Controllers;

use App;
use App\Dept_Category;
use App\Http\Controllers\Controller;
use App\Initial_Leave;
use App\Leave;
use App\Meeting;
use App\Leave_backup;
use App\Leave_Category;
use App\NewUser;
use App\Project_Category;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Storage;

class LeaveController extends Controller
{
	
	public function __construct()
    {
        $this->middleware(['auth', 'active']);
    }
    
    public function indexLeaveApply()
    {
        $ent_exdo = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')

        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2
                ) 
            ) as entitle_exdo')
        ])
        ->first();
        
        $select = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
      
        ->select([
            DB::raw('
            (
                select (
                    select initial_annual from users where id='.Auth::user()->id.' 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1                    
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainannual')
        ])
        ->first();

        

        $selectexdo = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=2
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainexdo')
        ])
        ->first();
 
	$exp_exdo = DB::table('initial_leave')
	
	->whereRaw('initial_leave.exp_date >= current_date')
	->orderByRaw('initial_leave.exp_date asc')
	->first();
	
		
        return View::make('leave.indexApply')->with(['select' => $select, 'selectexdo' => $selectexdo, 'ent_exdo' => $ent_exdo, 'exp_exdo' => $exp_exdo]);
    }

    public function indexNewApply()
    {
        $annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
      
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1                    
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as transactionAnnual')
        ])
        ->first();

        $user = User::find(auth::user()->id);

        $startDate = date_create($user->join_date);
        $endDate = date_create($user->end_date);

        $startYear = date('Y', strtotime($user->join_date));
        $endYear = date('Y', strtotime($user->end_date));

        if (auth::user()->emp_status === "Permanent") {
           $yearEnd = date('Y');
        } else {
            $yearEnd = $endYear;
        }

        $now = date_create();
        $now1 = date_create(date('Y').'-01-01');
        $now2 = date_create(date('Y').'-12-31');
            
       // date_create('2021-05-15') penambahan bulan terjadi
        // dd($endDate);
       
        if ($now <= $endDate) {
            $sekarang = $now;
        } else {
            $sekarang = $endDate;
        } 



        $daff = date_diff($startDate, $sekarang->modify('+5 day'))->format('%m')+(12*date_diff($startDate, $sekarang->modify('+5 day'))->format('%y'));

        $daffPermanent = date_diff($now1, $now->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now->modify('+5 day'))->format('%y'));

        $daffPermanent2 = date_diff($now1, $now2->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now2->modify('+5 day'))->format('%y'));

        $daffPermanent1 = 12 - $daffPermanent;        


        if ($daff <= $annual->transactionAnnual) {
            $newAnnual =  $annual->transactionAnnual;
        }else{
            $newAnnual = $daff;
        }       

        $totalAnnual = $newAnnual - $annual->transactionAnnual;

        $totalAnnualPermanent = $user->initial_annual - $annual->transactionAnnual;

        $totalAnnualPermanent1 = $totalAnnualPermanent - $daffPermanent1;

        // dd($sekarang);

        //-------------------------------------------------      

        $exdo = Initial_Leave::where('user_id', auth::user()->id)->pluck('initial');      

        $w = Initial_Leave::where('user_id', auth::user()->id)
                ->whereDATE('expired', '<=', Carbon::now())
                ->pluck('initial');

        $minusExdo = Leave::where('user_id', auth::user()->id)->where('leave_category_id', 2)->where('ap_hd', 1)->where('ap_gm', 1)->where('ver_hr', 1)->where('ap_hrd', 1)->pluck('total_day');

        $exdoHangus = $w->sum() - $minusExdo->sum();
        $exdoHangus1 = $minusExdo->sum() + $exdoHangus; 

        if ($w->sum() <= 0) {
            $sisaExdo = $exdo->sum() - $minusExdo->sum();
        } else {
           $sisaExdo = $exdo->sum() - $exdoHangus1;
        }           
        
        // dd($w);
        return view('leave.NewAnnual.indexNewAnnual',[
            'annual'      => $annual,
            'totalAnnual' => $totalAnnual,
            'totalAnnualPermanent1' => $totalAnnualPermanent1,
            'remainExdo'     => $sisaExdo,
            'startYear'     => $startYear,
            'yearEnd'       => $yearEnd,
            'user'      => $user,
            'exdo'      =>$exdo,
            'minusExdo' => $minusExdo,
            'w' => $w,
        ]);
    }

    public function indexDataExdo()
    {
         $model = Initial_Leave::where('user_id', auth::user()->id) 
                ->where('expired', '>=', date('Y-m-d', strtotime('-2 months'))) 
                ->orderBy('expired', 'asc')
                ->get();

      return Datatables::of($model)  
                ->addIndexColumn()               
                ->addColumn('difforHumans', function(Initial_Leave $initial){
                    $carbon =  Carbon::parse($initial->expired);
                    return $carbon->diffForHumans();
                })
                ->setRowClass('@if ($expired <= date("Y-m-d")){{ "danger" }}@endif')
                ->make(true);        
    }    

    public function createLeave()
    {
    	$department = dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name');

    	
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
   
        ->select([
            DB::raw('
            (
                select (
                    select initial_annual from users where id='.Auth::user()->id.' 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainannual')
        ])
        ->first();

        $annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
      
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1                    
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as transactionAnnual')
        ])
        ->first();

        // new rule
        $user = User::find(auth::user()->id);

        $startDate = date_create($user->join_date);
        $endDate = date_create($user->end_date);

        $startYear = date('Y', strtotime($user->join_date));
        $endYear = date('Y', strtotime($user->end_date));

        if (auth::user()->emp_status === "Permanent") {
           $yearEnd = date('Y');
        } else {
            $yearEnd = $endYear;
        }

        $now = date_create();
        $now1 = date_create(date('Y').'-01-01');
        $now2 = date_create(date('Y').'-12-31');

   

         if ($now <= $endDate) {
            $sekarang = $now;
        } else {
            $sekarang = $endDate;
        } 


        $daff = date_diff($startDate, $sekarang->modify('+5 day'))->format('%m')+(12*date_diff($startDate, $sekarang->modify('+5 day'))->format('%y'));

        $daffPermanent = date_diff($now1, $now->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now->modify('+5 day'))->format('%y'));

        $daffPermanent2 = date_diff($now1, $now2->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now2->modify('+5 day'))->format('%y'));

        $daffPermanent1 = 12 - $daffPermanent;        


        if ($daff <= $annual->transactionAnnual) {
            $newAnnual =  $annual->transactionAnnual;
        }else{
            $newAnnual = $daff;
        }       

        $totalAnnual = $newAnnual - $annual->transactionAnnual;

        $totalAnnualPermanent = $user->initial_annual - $annual->transactionAnnual;

        $totalAnnualPermanent1 = $totalAnnualPermanent - $daffPermanent1;

        $getAnnualBalance = null;

        if (auth::user()->emp_status === "Permanent")
        {
            $getAnnualBalance = $totalAnnualPermanent1;
        }else{
            $getAnnualBalance = $totalAnnual;
        }

        // dd($totalAnnualPermanent1);

        //

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=1
                ) 
            ) as entitle_ann')
        ])
        ->first();

      $leave_day = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                select (
                    SELECT datediff(end_leave_date, leave_date) FROM leave_transaction
					
                ) 
            ) as remainleaveday')
        ])
        ->first();

        $pro_category = User::where('users.koor', '=', 1)->where('active', 1)->orderBy('first_name', 'asc')->get();	
            foreach ($pro_category as $value)
                 $proe[$value->email] = $value->first_name.' '.$value->last_name;

         $pm_category = User::where('users.pm', '=', 1)->where('active', 1)->orderBy('first_name', 'asc')->get();    
            foreach ($pm_category as $value)
                $pmm[$value->email] = $value->first_name.' '.$value->last_name;

        $level_hrd =  User::where('level_hrd', '=', 'Senior Pipeline')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($level_hrd as $value)
                $level[$value->email] =  $value->first_name.' '.$value->last_name;

        $rickys = User::where('level_hrd', '=', 'Senior Technical')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($rickys as $value)
                $ricky['dede.aftafiandi@frameworks-studios.com'] =  $value->first_name.' '.$value->last_name;
      
        $ghea =   User::select('email')->where('active', 1)->where('hd', '=', '1')->where('dept_category_id', 6)->pluck('email');
       
        $infiniteApproved = User::where('active', 1)->where('infiniteApprove', 1)->where('dept_ApprovedHOD', auth::user()->dept_category_id)->value('email');      

        $johnReedel = User::select('email')->where('active', 1)->where('hd', 1)->where('dept_category_id', 7)->value('email');

        $provinsi = file_get_contents('http://dev.farizdotid.com/api/daerahindonesia/provinsi');
        $provinsii = json_decode($provinsi, true);
        $provinsii = $provinsii['provinsi']; 

        if (auth::user()->hd === 1) {
            $emailHoD = User::where('active', 1)->where('id', 279)->get(); 
            $labelEmailHOD = 'Verify by HR :';
        } else {
            if (auth::user()->dept_category_id === 4) {
                $emailHoD = User::where('active', 1)->where('dept_category_id', 6)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Produciton Manager :';
            } else {
                $emailHoD = User::where('active', 1)->where('dept_category_id', auth::user()->dept_category_id)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Head of Department :';
            }
            
        }

        foreach ($emailHoD as $Hod)
                $emailHOD[$Hod->email] = $Hod->first_name.' '.$Hod->last_name;

        if ($init_annual->remainannual)
        {
            return View::make('leave.create')->with([
            'leave' => $getAnnualBalance, 
            'department' => $department, 
            'labelEmailHOD' => $labelEmailHOD,
            'taken' => $taken->leavetaken, 
            'ent_annual' => $ent_annual->entitle_ann, 
            'proe' => $proe, 
            'pmm' => $pmm, 
            'level' => $level, 
            'ricky' => $ricky,
            'ghea' => $ghea, 
            'provinsii' => $provinsii,
             // 'leave_day' => $leave_day->remainleaveday
            'emailHOD' => $emailHOD,
            'infiniteApproved' => $infiniteApproved,
            'johnReedel' => $johnReedel,
            
			]);
        } else {
            return Redirect::route('leave/apply');
        }
    	
    }

    public function createExdo()
    {
        $department  = dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name');
        
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=2
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainexdo')
        ])
        ->first();

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=2
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_exdo = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2
                ) 
            ) as entitle_exdo')
        ])
        ->first();

         $pro_category = User::where('users.koor', '=', 1)->where('active', 1)->get();  
            foreach ($pro_category as $value)
                $proe[$value->email] = $value->first_name.' '.$value->last_name; 

         $pm_category = User::where('users.pm', '=', 1)->where('active', 1)->get();    
            foreach ($pm_category as $value)
                $pmm[$value->email] = $value->first_name.' '.$value->last_name;

        $level_hrd =  User::where('level_hrd', '=', 'Senior Pipeline')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($level_hrd as $value)
                $level[$value->email] =  $value->first_name.' '.$value->last_name;

        $rickys = User::where('level_hrd', '=', 'Senior Technical')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($rickys as $value)
                $ricky['dede.aftafiandi@frameworks-studios.com'] =  $value->first_name.' '.$value->last_name;

        $ghea =   User::select('email')->where('hd', '=', '1')->where('dept_category_id', 6)->pluck('email');

        $infiniteApproved = User::where('active', 1)->where('infiniteApprove', 1)->where('dept_ApprovedHOD', auth::user()->dept_category_id)->value('email');      

        $johnReedel = User::select('email')->where('active', 1)->where('hd', 1)->where('dept_category_id', 7)->value('email');

        $provinsi = file_get_contents('http://dev.farizdotid.com/api/daerahindonesia/provinsi');
        $provinsii = json_decode($provinsi, true);
        $provinsii = $provinsii['provinsi'];

        if (auth::user()->hd === 1) {
            $emailHoD = User::where('id', 279)->get(); 
            $labelEmailHOD = 'Verify by HR :';
        } else {
           if (auth::user()->dept_category_id === 4) {
                $emailHoD = User::where('active', 1)->where('dept_category_id', 6)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Produciton Manager :';
            } else {
                $emailHoD = User::where('active', 1)->where('dept_category_id', auth::user()->dept_category_id)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Head of Department :';
            }
           
        }

         foreach ($emailHoD as $Hod)
                $emailHOD[$Hod->email] = $Hod->first_name.' '.$Hod->last_name;
       
        if ($init_annual->remainexdo)
        {
            return View::make('leave.createExdo')->with([
                'leave' => $init_annual->remainexdo, 
                'department' => $department, 
                'labelEmailHOD' => $labelEmailHOD,
                'taken' => $taken->leavetaken, 
                'ent_exdo' => $ent_exdo->entitle_exdo, 
                'proe' => $proe, 
                'pmm' => $pmm, 
                'level' => $level, 
                'ghea' => $ghea, 
                'provinsii' => $provinsii,
                'emailHOD' => $emailHOD,
                'ricky' => $ricky,
                'infiniteApproved' => $infiniteApproved,
                'johnReedel' => $johnReedel
            ]);
        } else {
            return Redirect::route('leave/apply');
        }
        
    }

    public function createEtc()
    {
                 
        $department = dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name');
        $leave      = [];
        $list_leave = leave_Category::where('id', '>', '2')->orderBy('id','asc')->get();
            foreach ($list_leave as $value)
                $leave[$value->leave_category_name] = $value->leave_category_name;
        // dd($list_leave);
         $pro_category = User::where('users.koor', '=', 1)->where('active', 1)->get();  
            foreach ($pro_category as $value)
                $proe[$value->email] = $value->first_name.' '.$value->last_name; 

         $pm_category = User::where('users.pm', '=', 1)->where('active', 1)->get();    
            foreach ($pm_category as $value)
                $pmm[$value->email] = $value->first_name.' '.$value->last_name;

        $level_hrd =  User::where('level_hrd', '=', 'Senior Pipeline')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($level_hrd as $value)
                $level[$value->email] =  $value->first_name.' '.$value->last_name;

        $rickys = User::where('level_hrd', '=', 'Senior Technical')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($rickys as $value)
                $ricky['dede.aftafiandi@frameworks-studios.com'] =  $value->first_name.' '.$value->last_name;

        $ghea =   User::select('email')->where('hd', '=', '1')->where('dept_category_id', 6)->pluck('email');

        $infiniteApproved = User::where('active', 1)->where('infiniteApprove', 1)->where('dept_ApprovedHOD', auth::user()->dept_category_id)->value('email');      

        $johnReedel = User::select('email')->where('active', 1)->where('hd', 1)->where('dept_category_id', 7)->value('email');

        $provinsi = file_get_contents('http://dev.farizdotid.com/api/daerahindonesia/provinsi');
        $provinsii = json_decode($provinsi, true);
        $provinsii = $provinsii['provinsi'];

       if (auth::user()->hd === 1) {
            $emailHoD = User::where('id', 279)->get(); 
            $labelEmailHOD = 'Verify by HR :';
        } else {
            if (auth::user()->dept_category_id === 4) {
                $emailHoD = User::where('active', 1)->where('dept_category_id', 6)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Produciton Manager :';
            } else {
                $emailHoD = User::where('active', 1)->where('dept_category_id', auth::user()->dept_category_id)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Head of Department :';
            }
        }

         foreach ($emailHoD as $Hod)
                $emailHOD[$Hod->email] = $Hod->first_name.' '.$Hod->last_name;

        return View::make('leave.createEtc')->with([
            'leave' => $leave, 
            'labelEmailHOD' => $labelEmailHOD,
            'department' => $department, 
            'proe' => $proe, 
            'pmm' => $pmm, 
            'level' => $level, 
            'ghea' => $ghea, 
            'provinsii' => $provinsii,
            'ricky' => $ricky,
            'emailHOD'  => $emailHOD,
            'infiniteApproved' => $infiniteApproved,
            'johnReedel' => $johnReedel

        ]);
    }

    public function createAdvance()
    {
        $department = dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name');

        
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
   
        ->select([
            DB::raw('
            (
                select (
                    select initial_annual from users where id='.Auth::user()->id.' 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainannual')
        ])
        ->first();

        $annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
      
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1                    
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as transactionAnnual')
        ])
        ->first();

        // new rule
        $user = User::find(auth::user()->id);

        $startDate = date_create($user->join_date);
        $endDate = date_create($user->end_date);

        $startYear = date('Y', strtotime($user->join_date));
        $endYear = date('Y', strtotime($user->end_date));

        if (auth::user()->emp_status === "Permanent") {
           $yearEnd = date('Y');
        } else {
            $yearEnd = $endYear;
        }

        $now = date_create();
        $now1 = date_create(date('Y').'-01-01');
        $now2 = date_create(date('Y').'-12-31');

   

         if ($now <= $endDate) {
            $sekarang = $now;
        } else {
            $sekarang = $endDate;
        } 



        $daff = date_diff($startDate, $sekarang->modify('+5 day'))->format('%m')+(12*date_diff($startDate, $sekarang->modify('+5 day'))->format('%y'));

        $daffPermanent = date_diff($now1, $now->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now->modify('+5 day'))->format('%y'));

        $daffPermanent2 = date_diff($now1, $now2->modify('+5 day'))->format('%m')+(12*date_diff($now1, $now2->modify('+5 day'))->format('%y'));

        $daffPermanent1 = 12 - $daffPermanent;        


        if ($daff <= $annual->transactionAnnual) {
            $newAnnual =  $annual->transactionAnnual;
        }else{
            $newAnnual = $daff;
        }       

        $totalAnnual = $newAnnual - $annual->transactionAnnual;

        $totalAnnualPermanent = $user->initial_annual - $annual->transactionAnnual;

        $totalAnnualPermanent1 = $totalAnnualPermanent - $daffPermanent1;

        $getAnnualBalance = null;

        if (auth::user()->emp_status === "Permanent")
        {
            $getAnnualBalance = $totalAnnualPermanent1;
        }else{
            $getAnnualBalance = $totalAnnual;
        }

        // dd($init_annual);

        //

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=1
                ) 
            ) as entitle_ann')
        ])
        ->first();

      $leave_day = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                select (
                    SELECT datediff(end_leave_date, leave_date) FROM leave_transaction
                    
                ) 
            ) as remainleaveday')
        ])
        ->first();      

        $pro_category = User::where('users.koor', '=', 1)->where('active', 1)->orderBy('first_name', 'asc')->get(); 
            foreach ($pro_category as $value)
                 $proe[$value->email] = $value->first_name.' '.$value->last_name;

         $pm_category = User::where('users.pm', '=', 1)->where('active', 1)->orderBy('first_name', 'asc')->get();    
            foreach ($pm_category as $value)
                $pmm[$value->email] = $value->first_name.' '.$value->last_name;

        $level_hrd =  User::where('level_hrd', '=', 'Senior Pipeline')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($level_hrd as $value)
                $level[$value->email] =  $value->first_name.' '.$value->last_name;

        $rickys = User::where('level_hrd', '=', 'Senior Technical')->where('dept_category_id', 6)->where('active', 1)->get();
             foreach ($rickys as $value)
                $ricky['dede.aftafiandi@frameworks-studios.com'] =  $value->first_name.' '.$value->last_name;
        // dd($rickys);
        $ghea =   User::select('email')->where('hd', '=', '1')->where('dept_category_id', 6)->pluck('email');

        $provinsi = file_get_contents('http://dev.farizdotid.com/api/daerahindonesia/provinsi');
        $provinsii = json_decode($provinsi, true);
        $provinsii = $provinsii['provinsi']; 
      /*  $emailHOD = [
            'dede.aftafiandi@frameworks-studios.com' => 'HR Department'
        ]; */
        if (auth::user()->hd === 1) {
            $emailHoD = User::where('id', 279)->get(); 
            $labelEmailHOD = 'Verify by HR :';
        } else {
            if (auth::user()->dept_category_id === 4) {
                $emailHoD = User::where('active', 1)->where('dept_category_id', 6)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Produciton Manager :';
            } else {
                $emailHoD = User::where('active', 1)->where('dept_category_id', auth::user()->dept_category_id)->where('hd', 1)->get(); 
                $labelEmailHOD = 'Head of Department :';
            }
        }
         foreach ($emailHoD as $Hod)
                $emailHOD[$Hod->email] = $Hod->first_name.' '.$Hod->last_name;

        if ($init_annual->remainannual)
        {
            return View::make('leave.createAdvance')->with([
            'leave' => $init_annual->remainannual, 
            'department' => $department, 
            'taken' => $taken->leavetaken, 
            'labelEmailHOD' => $labelEmailHOD,
            'ent_annual' => $ent_annual->entitle_ann, 
            'proe' => $proe, 
            'pmm' => $pmm, 
            'level' => $level, 
            'ricky' => $ricky,
            'ghea' => $ghea, 
            'provinsii' => $provinsii,
            'emailHOD' => $emailHOD
            ]);
        } else {
            return Redirect::route('createAdvanceLeave');
        }
    }

    public function storeLeave(Request $request)
    {
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')   
        ->select([
            DB::raw('
            (
                select (
                    select initial_annual from users where id='.Auth::user()->id.' 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainannual')
        ])
        ->first();

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')    
        ->select([
            DB::raw('
            (
                (
                    select initial_annual from users where id='.Auth::user()->id.'
                ) 
            ) as entitle_ann')
        ])
        ->first();		
	
		 $leave_d = DB::table('leave_transaction')
		 ->whereRaw('Datediff(end_leave_date, leave_date)')
			->first();
		          
                $email_koor = null;
                $email_pm = null;
                $ap_gm      = 0;
                $date_ap_gm = null;
                $ap_pipeline = 0;
                $date_ap_pipeline = null;
                $ap_spv = 0;
                $date_ap_spv = null;
                $ap_koor = 0;
                $date_ap_koor = null;
                $ap_pm = 0;
                $date_ap_pm = null;
                $ap_producer = 0;
                $date_producer  = null;
                $ap_hd = 0;
                $date_ap_hd = null;
                // ---------------------
                $ap_infinite = 0;
                $date_ap_infinite = null;

        // dept_category 1 = IT
        if (auth::user()->dept_category_id === 1) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_hr -> HRD -> GM (mike) 
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }              
        }
        // dept_category 2 = Finance
        elseif (auth::user()->dept_category_id === 2) {
               if (auth::user()->hd === 1) {
                // HOD -> Sow Kim -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // ----------------------------------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                 // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');              
             }
        }
        // dept_category 3 = HRD
        elseif (auth::user()->dept_category_id === 3) {
               if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");               
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = 'hr.verification@frameworks-studios.com';
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
             }
        }
        //dept Category 4 = Operational ?
        elseif (auth::user()->dept_category_id === 4) {
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");                
                $email_pm = $request->input('sendtoPM'); 
        }
        //dept_category 5 = Facility
        elseif (auth::user()->dept_category_id === 5) {
               if (auth::user()->hd === 1) {
                // HOD -> John Reedel -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                // $ap_producer = 0;
                // $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept category 6 = Production
        elseif (auth::user()->dept_category_id === 6) {
            if (auth::user()->hd === 1) {
                // HOD -> Choonmeng -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 0;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 0;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -----------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                if (auth::user()->level_hrd != '0') {
                   if (auth::user()->level_hrd === 'Junior Pipeline') 
                   {        
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = null;
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   } elseif (auth::user()->level_hrd === 'Junior Technical') 
                   {  
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = null;
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   }elseif (auth::user()->level_hrd === 'Senior Pipeline') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }elseif (auth::user()->level_hrd === 'Senior Technical') {
                             $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }
                   
                } else {
                        if (auth::user()->producer === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        } 
                        elseif (auth::user()->pm === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        }
                        elseif (auth::user()->koor === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d"); 
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d"); 
                            $email_pm = $request->input('sendtoPM');           
                        }
                        elseif (auth::user()->spv === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");  
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");  
                            $email_koor = $request->input('sendto');  
                            $email_pm = $request->input('sendtoPM');              
                        }
                        else {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                            $email_pm = $request->input('sendtoPM');   
                        }                   
                }
                
                
             }
        }
        // dept category 7 = Production LS
        elseif (auth::user()->dept_category_id === 7) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d"); 

            } else {
                // Employee -> HOD-> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
            }
            
        }
        // dept category 8 = General
        elseif (auth::user()->dept_category_id === 8) {
               if (auth::user()->hd === 1) {
                    // HOD -> Ver_HR -> HRD 
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_hd = 1;
                    $date_ap_hd = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");
               } else {
                    // Employee -> HOD -> Ver_HR -> HRD
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");     
                    $email_pm = $request->input('sendtoPM');              
               }
               
        }
        // dept category 9 = Management ?
        elseif (auth::user()->dept_category_id === 9) {
            $ap_pipeline = 1;
            $date_ap_pipeline = date("Y-m-d");
            $ap_spv = 1;
            $date_ap_spv = date("Y-m-d");
            $ap_koor = 1;
            $date_ap_koor = date("Y-m-d");
            $ap_pm = 1;
            $date_ap_pm = date("Y-m-d");
            $ap_producer = 1;
            $date_producer  = date("Y-m-d");
            $ap_hd = 1;
            $date_ap_hd = date("Y-m-d");
            $ap_gm = 1;
            $date_ap_gm = date("Y-m-d");
        }
        ////////////////////////////////////////////
            $tahun_libur = db::table('view_off_year')->whereDate('date','>=', $request->input('leave_date'))->whereDate('date','<=', $request->input('end_leave_date'))->get();
            $tahuan = array();           
            foreach ($tahun_libur as $key => $tahuan_value) {
                $tahuan[] = $tahuan_value->date;
            }

            $awal_cuti = strtotime($request->input('leave_date'));
            $akhir_cuti = strtotime($request->input('end_leave_date'));
            $haricuti = array();         
            for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                  
                        $haricuti[] = $i;
                   
                }
            }
            $jumlah_cuti = count($haricuti)-count($tahuan);

            $remaining = $request->input('entitlement')-$jumlah_cuti;
            //////////////////////////////////////////////////////

            /////////////////////////////////////////////////
            $start_leaved       = $request->input('leave_date');
            $end_leaved         = $request->input('end_leave_date');
            $back_work_leaved   = $request->input('back_work');

        if ($start_leaved <= $end_leaved) {
           $get_end_leaved = $end_leaved;
        } else {
            $get_end_leaved = null;
        }

        if ($back_work_leaved > $end_leaved) {
            $get_bacl_work_leaved = $back_work_leaved;
        } else {
            $get_bacl_work_leaved = null;
        } 
       
        if ($request->input('nama_provin') === '11') {
            $nama_provins = 'Aceh';
        }elseif ($request->input('nama_provin') === '12') {
            $nama_provins = 'Sumatera Utara';
        }elseif ($request->input('nama_provin') === '13') {
            $nama_provins = 'Sumatera Barat';
        }elseif ($request->input('nama_provin') === '14') {
            $nama_provins = 'Riau';
        }elseif ($request->input('nama_provin') === '15') {
            $nama_provins = 'Jambi';
        }elseif ($request->input('nama_provin') === '16') {
            $nama_provins = 'Sumatera Selatan';
        }elseif ($request->input('nama_provin') === '17') {
            $nama_provins = 'Bengkulu';
        }elseif ($request->input('nama_provin') === '18') {
            $nama_provins = 'Lampung';
        }elseif ($request->input('nama_provin') === '19') {
            $nama_provins = 'Kepulauan Bangka Belitung';
        }elseif ($request->input('nama_provin') === '21') {
            $nama_provins = 'Kepulauan Riau';
        }elseif ($request->input('nama_provin') === '31') {
            $nama_provins = 'Dki Jakarta';
        }elseif ($request->input('nama_provin') === '32') {
            $nama_provins = 'Jawa Barat';
        }elseif ($request->input('nama_provin') === '33') {
            $nama_provins = 'Jawa Tengah';
        }elseif ($request->input('nama_provin') === '34') {
            $nama_provins = 'Di Yogyakarta';
        }elseif ($request->input('nama_provin') === '35') {
            $nama_provins = 'Jawa Timur';
        }elseif ($request->input('nama_provin') === '36') {
            $nama_provins = 'Banten';
        }elseif ($request->input('nama_provin') === '51') {
            $nama_provins = 'Bali';
        }elseif ($request->input('nama_provin') === '52') {
            $nama_provins = 'Nusa Tenggara Barat';
        }elseif ($request->input('nama_provin') === '53') {
            $nama_provins = 'Nusa Tenggara Timur';
        }elseif ($request->input('nama_provin') === '61') {
            $nama_provins = 'Kalimantan Barat';
        }elseif ($request->input('nama_provin') === '62') {
            $nama_provins = 'Kalimantan Tengah';
        }elseif ($request->input('nama_provin') === '63') {
            $nama_provins = 'Kalimantan Selatan';
        }elseif ($request->input('nama_provin') === '64') {
            $nama_provins = 'Kalimantan Timur';
        }elseif ($request->input('nama_provin') === '65') {
            $nama_provins = 'Kalimantan Utara';
        }elseif ($request->input('nama_provin') === '71') {
            $nama_provins = 'Sulawesi Utara';
        }elseif ($request->input('nama_provin') === '72') {
            $nama_provins = 'Sulawesi Tengah';
        }elseif ($request->input('nama_provin') === '73') {
            $nama_provins = 'Sulawesi Selatan';
        }elseif ($request->input('nama_provin') === '74') {
            $nama_provins = 'Sulawesi Tenggara';
        }elseif ($request->input('nama_provin') === '75') {
            $nama_provins = 'Gorontalo';
        }elseif ($request->input('nama_provin') === '76') {
            $nama_provins = 'Sulawesi Barat';
        }elseif ($request->input('nama_provin') === '81') {
            $nama_provins = 'Maluku';
        }elseif ($request->input('nama_provin') === '82') {
            $nama_provins = 'Maluku Utara';
        }elseif ($request->input('nama_provin') === '91') {
            $nama_provins = 'Papua Barat';
        }elseif ($request->input('nama_provin') === '94') {
            $nama_provins = 'Papua';
        }else {
            $nama_provins = $request->input('nama_provin');
        }

         
        $rules = [
            'leave_date'        => 'required|date',
            'end_leave_date'    => 'required|date',
            'back_work'         => 'required|date',
            'reason'            => 'required|max:50'
           /* 'total_day'         => 'required|numeric',*/
           /* 'remain'            => 'required|numeric|min:0'*/
            ];
       
        $data = [
            'user_id'                    => Auth::user()->id,
            'leave_category_id'          => '1',
            'request_by'                 => Auth::user()->first_name.' '.Auth::user()->last_name,
            'request_nik'                => Auth::user()->nik,
            'request_position'           => Auth::user()->position,
            'request_join_date'          => Auth::user()->join_date,
            'request_dept_category_name' => dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name'),
            'period'                     => date('Y'),
            'leave_date'                 => $request->input('leave_date'),
            'end_leave_date'             => $get_end_leaved,
            'back_work'                  => $get_bacl_work_leaved,
            'total_day'                  => $jumlah_cuti,
            'taken'                      => $taken->leavetaken,
            'entitlement'                => $ent_annual->entitle_ann,
            'pending'                    => $init_annual->remainannual,
            'remain'                     => $remaining,
            'ap_hd'                      => $ap_hd,
            'ap_gm'                      => $ap_gm,
            'date_ap_hd'                 => $date_ap_hd,
            'date_ap_gm'                 => $date_ap_gm,
            'ver_hr'                     => '0',
            'ap_koor'                    => $ap_koor,
            'ap_spv'                     => $ap_spv,
            'ap_pm'                     => $ap_pm,
            'ap_producer'               => $ap_producer,
            'ap_pipeline'               => $ap_pipeline,
            'ap_Infinite'               => $ap_infinite,
            'date_ap_Infinite'          => $date_ap_infinite,
            'date_ap_koor'              => $date_ap_koor,
            'date_ap_spv'               => $date_ap_spv,
            'date_ap_pm'                => $date_ap_pm,
            'date_ap_pipeline'          => $date_ap_pipeline,
            'email_koor'                => $email_koor,
            'email_pm'                  => $email_pm,
            'reason_leave'              => strtolower($request->input('reason')),
            'r_departure'               => $nama_provins,
            'r_after_leaving'           => $request->input('nama_city'),
            'plan_leave'                => $request->input('rencana'),
            'agreement'                 => $request->input('agree'),
            'resendmail'                => 2,
            ];
            // dd($data);
			$validator = Validator::make($request->all(), $rules);
          
		    if ($validator->fails()) {
		        return Redirect::route('leave/create')
		            ->withErrors($validator)
		            ->withInput();
		    } else {
               if ($get_bacl_work_leaved != null) {
                   if ($get_end_leaved != null) {
                        if ($remaining < 0) 
                        {
                         Session::flash('getError', Lang::get('messages.annual_balance_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                         return back();
                        } else {
                        // Leave::insert($data);                 
                        Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Data Leave Transaction']));
                        // $this->sendEmail3();
                        return Redirect::route('leave/transaction');
                        }
                   } else {
                      Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
                   }                
               } else {
                   Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
               }               
		    }
    }      

  public function sendEmail3()
  {
    $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()
            ->orderBy('leave_transaction.id','des')
            ->first();

    $subject = 'Verify Request Leave Application - '.$select->request_by.'';

        if ($select->email_koor != null) {
            Mail::send('email.appMail', ['select' => $select], function($message) use ($select, $subject)
                    {
                
                     $message->to($select->email_koor)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
        }else{
            
            if ($select->email_pm != null) {
                // untuk Officer sent to Head Departement
                Mail::send('email.appMail', ['select' => $select], function($message) use ($select, $subject)
                    {
                
                     $message->to($select->email_pm)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                // untuk Head Department
                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($select, $subject)
                    {
                    
                    $message->to('hr.verification@frameworks-studios.com')->subject($subject);
                    $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            }
           
        }
   

  }

  public function sendmail2()
  {    
    $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()
                        ->orderBy('leave_transaction.id','des')
                        ->first();
            
    $subject = 'Verify Request Leave Application - '.$select->request_by.'';

    if (auth::user()->dept_category_id === 1) {
        if (auth::user()->hd === 1) {
              $gm = NewUser::joinDeptCategory()
                        ->where('users.gm', '=', 1)                       
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($gm, $subject)
                    {
                
                     $message->to($gm->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         } else {
               $IT = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 1)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($IT, $subject)
                    {
                
                     $message->to($IT->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    }); 
         }
          
       } 
    elseif (auth::user()->dept_category_id === 2) {
         if (auth::user()->hd === 1) {
             $gm = NewUser::joinDeptCategory()
                        ->where('users.gm', '=', 1)                       
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($gm, $subject)
                    {
                
                     $message->to($gm->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         } else {
             $acc = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 2)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($acc, $subject)
                    {
                
                     $message->to($acc->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         }
         
     }
     elseif (auth::user()->dept_category_id === 3) {
           if (auth::user()->hd === 1) {
               $gm = NewUser::joinDeptCategory()
                        ->where('users.gm', '=', 1)                       
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($gm, $subject)
                    {
                
                     $message->to($gm->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
           } else {
               $hr = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 3)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($hr, $subject)
                    {
                
                     $message->to($hr->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
           }
           
      }
      elseif (auth::user()->dept_category_id === 4) {
           $operation = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 6)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($operation, $subject)
                    {
                
                     $message->to($operation->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
      }
      elseif (auth::user()->dept_category_id === 5) {
         if (auth::user()->hd === 1) {
            $gm = NewUser::joinDeptCategory()
                        ->where('users.gm', '=', 1)                       
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($gm, $subject)
                    {
                
                     $message->to($gm->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         } else {
             $facility = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 6)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($facility, $subject)
                    {
                
                     $message->to($facility->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         }
         
      }
      elseif (auth::user()->dept_category_id === 6) {
            if (auth::user()->hd === 1) {
                $gm = NewUser::joinDeptCategory()
                        ->where('users.gm', '=', 1)                       
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($gm, $subject)
                    {
                
                     $message->to($gm->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
             } else {
                 if (auth::user()->level_hrd != '0') {
                         if (auth::user()->level_hrd === 'Junior Pipeline') {
                             $juniorPipeline = NewUser::joinDeptCategory()                        
                                    ->where('users.dept_category_id', 6)
                                    ->where('level_hrd', '=', 'Senior Pipeline')
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($juniorPipeline, $subject)
                                {
                            
                                 $message->to($juniorPipeline->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                         } elseif (auth::user()->level_hrd === 'Technical Director' or auth::user()->level_hrd === 'Senior Technical' or auth::user()->level_hrd === 'Senior Pipeline') {
                             $Pipeline = NewUser::joinDeptCategory()
                                    ->where('users.hd', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($Pipeline, $subject)
                                {
                            
                                 $message->to($Pipeline->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                         }                 
                 } 
                 else {
                        if (auth::user()->producer === 1) {
                            $producer = NewUser::joinDeptCategory()
                                    ->where('users.hd', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($producer, $subject)
                                {
                            
                                 $message->to($producer->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                        }
                        elseif (auth::user()->pm === 1) {
                             $pm = NewUser::joinDeptCategory()
                                    ->where('users.hd', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($pm, $subject)
                                {
                            
                                 $message->to($pm->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                        }
                        elseif (auth::user()->koor === 1) {
                            $koor = NewUser::joinDeptCategory()
                                    ->where('users.pm', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($koor, $subject)
                                {
                            
                                 $message->to($koor->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                        } 
                        elseif (auth::user()->spv === 1) {
                           $spv = NewUser::joinDeptCategory()
                                    ->where('users.koor', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($spv, $subject)
                                {
                            
                                 $message->to($spv->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                        }
                        else{
                            $karyawanProduction = NewUser::joinDeptCategory()
                                    ->where('users.koor', '=', 1)
                                    ->where('users.dept_category_id', 6)
                                    ->first();         

                                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($karyawanProduction, $subject)
                                {
                            
                                 $message->to($karyawanProduction->email)->subject($subject);
                                 $message->from('wis_system@frameworks-studios.com', 'WIS');
                                });
                        }            
                     }                
             }                      
      }
      elseif (auth::user()->dept_category_id === 7) {
                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($subject)
                    {                
                     $message->to('hr.verification@frameworks-studios.com')->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
      }
      elseif (auth::user()->dept_category_id === 8) {
         if (auth::user()->hd === 1) {
                Mail::send('email.leaveMail', ['select' => $select], function($message) use ($subject)
                    {                
                     $message->to('hr.verification@frameworks-studios.com')->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         } else {
            $pr = NewUser::joinDeptCategory()
                        ->where('users.hd', '=', 1)
                        ->where('users.dept_category_id', 8)
                        ->first();         

                    Mail::send('email.leaveMail', ['select' => $select], function($message) use ($pr, $subject)
                    {
                
                     $message->to($pr->email)->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
         }
         
      }
      elseif (auth::user()->dept_category_id === 9) {
         Mail::send('email.leaveMail', ['select' => $select], function($message) use ($subject)
                    {                
                     $message->to('hr.verification@frameworks-studios.com')->subject($subject);
                     $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
      }       
    }

    public function storeLeaveExdo(Request $request)
    {
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                select (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=2
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainexdo')
        ])
        ->first();

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=2
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_exdo = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(initial), 0) from initial_leave where user_id='.Auth::user()->id.' and leave_category_id=2
                ) 
            ) as entitle_exdo')
        ])
        ->first();

           $email_koor = null;
                $email_pm = null;
                $ap_gm      = 0;
                $date_ap_gm = null;
                $ap_pipeline = 0;
                $date_ap_pipeline = null;
                $ap_spv = 0;
                $date_ap_spv = null;
                $ap_koor = 0;
                $date_ap_koor = null;
                $ap_pm = 0;
                $date_ap_pm = null;
                $ap_producer = 0;
                $date_producer  = null;
                $ap_hd = 0;
                $date_ap_hd = null;
                //---------------------------
                $ap_infinite = 1;
                $date_ap_infinite = null;

        // dept_category 1 = IT
        if (auth::user()->dept_category_id === 1) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_hr -> HRD -> GM (mike) 
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }              
        }
        // dept_category 2 = Finance
        elseif (auth::user()->dept_category_id === 2) {
               if (auth::user()->hd === 1) {
                // HOD -> Sow Kim -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // ----------------------------------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                 // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        // dept_category 3 = HRD
        elseif (auth::user()->dept_category_id === 3) {
               if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");               
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept Category 4 = Operational ?
        elseif (auth::user()->dept_category_id === 4) {
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $ap_hd = 0;
                $date_ap_hd = date("Y-m-d");  
                $email_pm = $request->input('sendtoPM');   
        }
        //dept_category 5 = Facility
        elseif (auth::user()->dept_category_id === 5) {
               if (auth::user()->hd === 1) {
                // HOD -> John Reedel -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                // $ap_producer = 0;
                // $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept category 6 = Production
        elseif (auth::user()->dept_category_id === 6) {
            if (auth::user()->hd === 1) {
                // HOD -> Choonmeng -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -----------------------
                $ap_infinite = 0;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                if (auth::user()->level_hrd != '0') {
                   if (auth::user()->level_hrd === 'Junior Pipeline') 
                   {        
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = null;
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   } elseif (auth::user()->level_hrd === 'Junior Technical') 
                   {  
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = null;
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   }elseif (auth::user()->level_hrd === 'Senior Pipeline') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }elseif (auth::user()->level_hrd === 'Senior Technical') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }
                   
                } else {
                        if (auth::user()->producer === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        } 
                        elseif (auth::user()->pm === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        }
                        elseif (auth::user()->koor === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d"); 
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d"); 
                            $email_pm = $request->input('sendtoPM');           
                        }
                        elseif (auth::user()->spv === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");  
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");  
                            $email_koor = $request->input('sendto');  
                            $email_pm = $request->input('sendtoPM');              
                        }
                        else {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                            $email_pm = $request->input('sendtoPM');   
                        }                   
                }
                
                
             }
        }
        // dept category 7 = Production LS
        elseif (auth::user()->dept_category_id === 7) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");                
            } else {
                // Employee -> HOD-> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
            }
            
        }
        // dept category 8 = General
        elseif (auth::user()->dept_category_id === 8) {
               if (auth::user()->hd === 1) {
                    // HOD -> Ver_HR -> HRD 
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_hd = 1;
                    $date_ap_hd = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");
               } else {
                    // Employee -> HOD -> Ver_HR -> HRD
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");     
                    $email_pm = $request->input('sendtoPM');              
               }
               
        }
        // dept category 9 = Management ?
        elseif (auth::user()->dept_category_id === 9) {
            $ap_pipeline = 1;
            $date_ap_pipeline = date("Y-m-d");
            $ap_spv = 1;
            $date_ap_spv = date("Y-m-d");
            $ap_koor = 1;
            $date_ap_koor = date("Y-m-d");
            $ap_pm = 1;
            $date_ap_pm = date("Y-m-d");
            $ap_producer = 1;
            $date_producer  = date("Y-m-d");
            $ap_hd = 1;
            $date_ap_hd = date("Y-m-d");
            $ap_gm = 1;
            $date_ap_gm = date("Y-m-d");
        }
        ////////////////////////////////////////////
            $tahun_libur = db::table('view_off_year')->whereDate('date','>=', $request->input('leave_date'))->whereDate('date','<=', $request->input('end_leave_date'))->get();
            $tahuan = array();           
            foreach ($tahun_libur as $key => $tahuan_value) {
                $tahuan[] = $tahuan_value->date;
            }

            $awal_cuti = strtotime($request->input('leave_date'));
            $akhir_cuti = strtotime($request->input('end_leave_date'));
            $haricuti = array();         
            for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                  
                        $haricuti[] = $i;
                   
                }
            }
            $jumlah_cuti = count($haricuti)-count($tahuan);

              $remaining = $request->input('entitlement')-$jumlah_cuti;
            //////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
        $start_leaved       = $request->input('leave_date');
        $end_leaved         = $request->input('end_leave_date');
        $back_work_leaved   = $request->input('back_work');

        if ($start_leaved <= $end_leaved) {
           $get_end_leaved = $end_leaved;
        } else {
            $get_end_leaved = null;
        }

        if ($back_work_leaved > $end_leaved) {
            $get_bacl_work_leaved = $back_work_leaved;
        } else {
            $get_bacl_work_leaved = null;
        }


       if ($request->input('nama_provin') === '11') {
            $nama_provins = 'Aceh';
        }elseif ($request->input('nama_provin') === '12') {
            $nama_provins = 'Sumatera Utara';
        }elseif ($request->input('nama_provin') === '13') {
            $nama_provins = 'Sumatera Barat';
        }elseif ($request->input('nama_provin') === '14') {
            $nama_provins = 'Riau';
        }elseif ($request->input('nama_provin') === '15') {
            $nama_provins = 'Jambi';
        }elseif ($request->input('nama_provin') === '16') {
            $nama_provins = 'Sumatera Selatan';
        }elseif ($request->input('nama_provin') === '17') {
            $nama_provins = 'Bengkulu';
        }elseif ($request->input('nama_provin') === '18') {
            $nama_provins = 'Lampung';
        }elseif ($request->input('nama_provin') === '19') {
            $nama_provins = 'Kepulauan Bangka Belitung';
        }elseif ($request->input('nama_provin') === '21') {
            $nama_provins = 'Kepulauan Riau';
        }elseif ($request->input('nama_provin') === '31') {
            $nama_provins = 'Dki Jakarta';
        }elseif ($request->input('nama_provin') === '32') {
            $nama_provins = 'Jawa Barat';
        }elseif ($request->input('nama_provin') === '33') {
            $nama_provins = 'Jawa Tengah';
        }elseif ($request->input('nama_provin') === '34') {
            $nama_provins = 'Di Yogyakarta';
        }elseif ($request->input('nama_provin') === '35') {
            $nama_provins = 'Jawa Timur';
        }elseif ($request->input('nama_provin') === '36') {
            $nama_provins = 'Banten';
        }elseif ($request->input('nama_provin') === '51') {
            $nama_provins = 'Bali';
        }elseif ($request->input('nama_provin') === '52') {
            $nama_provins = 'Nusa Tenggara Barat';
        }elseif ($request->input('nama_provin') === '53') {
            $nama_provins = 'Nusa Tenggara Timur';
        }elseif ($request->input('nama_provin') === '61') {
            $nama_provins = 'Kalimantan Barat';
        }elseif ($request->input('nama_provin') === '62') {
            $nama_provins = 'Kalimantan Tengah';
        }elseif ($request->input('nama_provin') === '63') {
            $nama_provins = 'Kalimantan Selatan';
        }elseif ($request->input('nama_provin') === '64') {
            $nama_provins = 'Kalimantan Timur';
        }elseif ($request->input('nama_provin') === '65') {
            $nama_provins = 'Kalimantan Utara';
        }elseif ($request->input('nama_provin') === '71') {
            $nama_provins = 'Sulawesi Utara';
        }elseif ($request->input('nama_provin') === '72') {
            $nama_provins = 'Sulawesi Tengah';
        }elseif ($request->input('nama_provin') === '73') {
            $nama_provins = 'Sulawesi Selatan';
        }elseif ($request->input('nama_provin') === '74') {
            $nama_provins = 'Sulawesi Tenggara';
        }elseif ($request->input('nama_provin') === '75') {
            $nama_provins = 'Gorontalo';
        }elseif ($request->input('nama_provin') === '76') {
            $nama_provins = 'Sulawesi Barat';
        }elseif ($request->input('nama_provin') === '81') {
            $nama_provins = 'Maluku';
        }elseif ($request->input('nama_provin') === '82') {
            $nama_provins = 'Maluku Utara';
        }elseif ($request->input('nama_provin') === '91') {
            $nama_provins = 'Papua Barat';
        }elseif ($request->input('nama_provin') === '94') {
            $nama_provins = 'Papua';
        }else {
             $nama_provins = $request->input('nama_provin');
        }      

        $rules = [
            'leave_date'        => 'required',
            'end_leave_date'    => 'required',
            'back_work'         => 'required',
            'reason'            => 'required|max:50'
           /* 'total_day'         => 'required|numeric',
            'remain'            => 'required|numeric|min:0'*/
            ];

        $data = [
            'user_id'                    => Auth::user()->id,
            'leave_category_id'          => '2',
            'request_by'                 => Auth::user()->first_name.' '.Auth::user()->last_name,
            'request_nik'                => Auth::user()->nik,
            'request_position'           => Auth::user()->position,
            'request_join_date'          => Auth::user()->join_date,
            'request_dept_category_name' => dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name'),
            'period'                     => date('Y'),
            'leave_date'                 => $request->input('leave_date'),
            'end_leave_date'             => $get_end_leaved,
            'back_work'                  => $get_bacl_work_leaved,
            'total_day'                  => $jumlah_cuti,
            'taken'                      => null,     
            'entitlement'                => null,
            'pending'                    => $init_annual->remainexdo,
            'remain'                     => $remaining,
            'ap_hd'                      => $ap_hd,
            'ap_gm'                      => $ap_gm,
            'date_ap_hd'                 => $date_ap_hd,
            'date_ap_gm'                 => $date_ap_gm,
            'ver_hr'                     => '0',
            'ap_koor'                    => $ap_koor,
            'ap_spv'                     => $ap_spv,
            'ap_pm'                     => $ap_pm,
            'ap_producer'               => $ap_producer,
            'ap_pipeline'               => $ap_pipeline,
            'date_ap_koor'               => $date_ap_koor,
            'date_ap_spv'                => $date_ap_spv,
            'date_ap_pm'                => $date_ap_pm,
            'date_ap_pipeline'          => $date_ap_pipeline,
            'email_koor'                => $email_koor,
            'email_pm'                  => $email_pm,
            'reason_leave'              => strtolower($request->input('reason')),
            'r_departure'               => $nama_provins,
            'r_after_leaving'           => $request->input('nama_city'),
            'plan_leave'                => $request->input('rencana'),
            'agreement'                 => $request->input('agree'),  
            'ap_Infinite'               => $ap_infinite,
            'date_ap_Infinite'          => $date_ap_infinite, 
            'resendmail'                => 2,      
            ];

            // dd($data);
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return Redirect::route('leave/createExdo')
                    ->withErrors($validator)
                    ->withInput();
            } else {               
              if ($get_bacl_work_leaved != null) {
                   if ($get_end_leaved != null) {
                        if ($remaining < 0) 
                        {
                         Session::flash('getError', Lang::get('messages.annual_balance_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                         return back();
                        } else {
                        // Leave::insert($data);                 
                        Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Data Leave Transaction']));
                        // $this->sendEmail3();
                        return Redirect::route('leave/transaction');
                        }
                   } else {
                      Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
                   }                
               } else {
                   Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
               }       
            }
    }


    public function storeLeaveEtc(Request $request)
    {
         $email_koor = null;
                $email_pm = null;
                $ap_gm      = 0;
                $date_ap_gm = null;
                $ap_pipeline = 0;
                $date_ap_pipeline = null;
                $ap_spv = 0;
                $date_ap_spv = null;
                $ap_koor = 0;
                $date_ap_koor = null;
                $ap_pm = 0;
                $date_ap_pm = null;
                $ap_producer = 0;
                $date_producer  = null;
                $ap_hd = 0;
                $date_ap_hd = null;
                //----------------------
                $ap_infinite = 1;
                $date_ap_infinite = null;

       // dept_category 1 = IT
        if (auth::user()->dept_category_id === 1) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_hr -> HRD -> GM (mike) 
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }              
        }
        // dept_category 2 = Finance
        elseif (auth::user()->dept_category_id === 2) {
               if (auth::user()->hd === 1) {
                // HOD -> Sow Kim -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // ----------------------------------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                 // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        // dept_category 3 = HRD
        elseif (auth::user()->dept_category_id === 3) {
               if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");               
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept Category 4 = Operational ?
        elseif (auth::user()->dept_category_id === 4) {
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $ap_hd = 0;
                $date_ap_hd = date("Y-m-d"); 
                $email_pm = $request->input('sendtoPM');    
        }
        //dept_category 5 = Facility
        elseif (auth::user()->dept_category_id === 5) {
               if (auth::user()->hd === 1) {
                // HOD -> John Reedel -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                // $ap_producer = 0;
                // $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept category 6 = Production
        elseif (auth::user()->dept_category_id === 6) {
            if (auth::user()->hd === 1) {
                // HOD -> Choonmeng -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -----------------------
                $ap_infinite = 0;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                if (auth::user()->level_hrd != '0') {
                   if (auth::user()->level_hrd === 'Junior Pipeline') 
                   {        
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   } elseif (auth::user()->level_hrd === 'Junior Technical') 
                   {  
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   }elseif (auth::user()->level_hrd === 'Senior Pipeline') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }elseif (auth::user()->level_hrd === 'Senior Technical') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }
                   
                } else {
                        if (auth::user()->producer === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        } 
                        elseif (auth::user()->pm === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        }
                        elseif (auth::user()->koor === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d"); 
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d"); 
                            $email_pm = $request->input('sendtoPM');           
                        }
                        elseif (auth::user()->spv === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");  
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");  
                            $email_koor = $request->input('sendto');  
                            $email_pm = $request->input('sendtoPM');              
                        }
                        else {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                            $email_pm = $request->input('sendtoPM');   
                        }                   
                }
                
                
             }
        }
        // dept category 7 = Production LS
        elseif (auth::user()->dept_category_id === 7) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");                
            } else {
                // Employee -> HOD-> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
            }
            
        }
        // dept category 8 = General
        elseif (auth::user()->dept_category_id === 8) {
               if (auth::user()->hd === 1) {
                    // HOD -> Ver_HR -> HRD 
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_hd = 1;
                    $date_ap_hd = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");
               } else {
                    // Employee -> HOD -> Ver_HR -> HRD
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");     
                    $email_pm = $request->input('sendtoPM');              
               }
               
        }
        // dept category 9 = Management ?
        elseif (auth::user()->dept_category_id === 9) {
            $ap_pipeline = 1;
            $date_ap_pipeline = date("Y-m-d");
            $ap_spv = 1;
            $date_ap_spv = date("Y-m-d");
            $ap_koor = 1;
            $date_ap_koor = date("Y-m-d");
            $ap_pm = 1;
            $date_ap_pm = date("Y-m-d");
            $ap_producer = 1;
            $date_producer  = date("Y-m-d");
            $ap_hd = 1;
            $date_ap_hd = date("Y-m-d");
            $ap_gm = 1;
            $date_ap_gm = date("Y-m-d");
        }

         ////////////////////////////////////////////
            $tahun_libur = db::table('view_off_year')->whereDate('date','>=', $request->input('leave_date'))->whereDate('date','<=', $request->input('end_leave_date'))->get();
            $tahuan = array();           
            foreach ($tahun_libur as $key => $tahuan_value) {
                $tahuan[] = $tahuan_value->date;
            }

            $awal_cuti = strtotime($request->input('leave_date'));
            $akhir_cuti = strtotime($request->input('end_leave_date'));
            $haricuti = array();         
            for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                  
                        $haricuti[] = $i;
                   
                }
            }
            $jumlah_cuti = count($haricuti)-count($tahuan);
            //////////////////////////////////////////////////////      
            ////////////////////////////////////////////////////////
            $start_leaved       = $request->input('leave_date');
            $end_leaved         = $request->input('end_leave_date');
            $back_work_leaved   = $request->input('back_work');

            if ($start_leaved <= $end_leaved) {
               $get_end_leaved = $end_leaved;
            } else {
                $get_end_leaved = null;
            }

            if ($back_work_leaved > $end_leaved) {
                $get_bacl_work_leaved = $back_work_leaved;
            } else {
                $get_bacl_work_leaved = null;
            }

       if ($request->input('nama_provin') === '11') {
            $nama_provins = 'Aceh';
        }elseif ($request->input('nama_provin') === '12') {
            $nama_provins = 'Sumatera Utara';
        }elseif ($request->input('nama_provin') === '13') {
            $nama_provins = 'Sumatera Barat';
        }elseif ($request->input('nama_provin') === '14') {
            $nama_provins = 'Riau';
        }elseif ($request->input('nama_provin') === '15') {
            $nama_provins = 'Jambi';
        }elseif ($request->input('nama_provin') === '16') {
            $nama_provins = 'Sumatera Selatan';
        }elseif ($request->input('nama_provin') === '17') {
            $nama_provins = 'Bengkulu';
        }elseif ($request->input('nama_provin') === '18') {
            $nama_provins = 'Lampung';
        }elseif ($request->input('nama_provin') === '19') {
            $nama_provins = 'Kepulauan Bangka Belitung';
        }elseif ($request->input('nama_provin') === '21') {
            $nama_provins = 'Kepulauan Riau';
        }elseif ($request->input('nama_provin') === '31') {
            $nama_provins = 'Dki Jakarta';
        }elseif ($request->input('nama_provin') === '32') {
            $nama_provins = 'Jawa Barat';
        }elseif ($request->input('nama_provin') === '33') {
            $nama_provins = 'Jawa Tengah';
        }elseif ($request->input('nama_provin') === '34') {
            $nama_provins = 'Di Yogyakarta';
        }elseif ($request->input('nama_provin') === '35') {
            $nama_provins = 'Jawa Timur';
        }elseif ($request->input('nama_provin') === '36') {
            $nama_provins = 'Banten';
        }elseif ($request->input('nama_provin') === '51') {
            $nama_provins = 'Bali';
        }elseif ($request->input('nama_provin') === '52') {
            $nama_provins = 'Nusa Tenggara Barat';
        }elseif ($request->input('nama_provin') === '53') {
            $nama_provins = 'Nusa Tenggara Timur';
        }elseif ($request->input('nama_provin') === '61') {
            $nama_provins = 'Kalimantan Barat';
        }elseif ($request->input('nama_provin') === '62') {
            $nama_provins = 'Kalimantan Tengah';
        }elseif ($request->input('nama_provin') === '63') {
            $nama_provins = 'Kalimantan Selatan';
        }elseif ($request->input('nama_provin') === '64') {
            $nama_provins = 'Kalimantan Timur';
        }elseif ($request->input('nama_provin') === '65') {
            $nama_provins = 'Kalimantan Utara';
        }elseif ($request->input('nama_provin') === '71') {
            $nama_provins = 'Sulawesi Utara';
        }elseif ($request->input('nama_provin') === '72') {
            $nama_provins = 'Sulawesi Tengah';
        }elseif ($request->input('nama_provin') === '73') {
            $nama_provins = 'Sulawesi Selatan';
        }elseif ($request->input('nama_provin') === '74') {
            $nama_provins = 'Sulawesi Tenggara';
        }elseif ($request->input('nama_provin') === '75') {
            $nama_provins = 'Gorontalo';
        }elseif ($request->input('nama_provin') === '76') {
            $nama_provins = 'Sulawesi Barat';
        }elseif ($request->input('nama_provin') === '81') {
            $nama_provins = 'Maluku';
        }elseif ($request->input('nama_provin') === '82') {
            $nama_provins = 'Maluku Utara';
        }elseif ($request->input('nama_provin') === '91') {
            $nama_provins = 'Papua Barat';
        }elseif ($request->input('nama_provin') === '94') {
            $nama_provins = 'Papua';
        }else {
             $nama_provins = $request->input('nama_provin');
        }

        $rules = [
            'leave_date'        => 'required',
            'end_leave_date'    => 'required',
            'back_work'         => 'required',
            'reason'            => 'required|max:100'
            /*'total_day'         => 'required|numeric'*/
            ];
       
        $data = [
            'user_id'                    => Auth::user()->id,       
            'leave_category_id'          => leave_Category::where('leave_category_name', $request->input('leave_category_id'))->value('id'),
            'request_by'                 => Auth::user()->first_name.' '.Auth::user()->last_name,
            'request_nik'                => Auth::user()->nik,
            'request_position'           => Auth::user()->position,
            'request_join_date'          => Auth::user()->join_date,
            'request_dept_category_name' => dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name'),
            'period'                     => date('Y'),
            'leave_date'                 => $request->input('leave_date'),
            'end_leave_date'             => $get_end_leaved,
            'back_work'                  => $get_bacl_work_leaved,
          /*  'total_day'                  => $request->input('total_day'),*/
            'total_day'                  => $jumlah_cuti,
            'taken'                      => null,
            'entitlement'                => null,
            'pending'                    => null,
            'ap_hd'                      => $ap_hd,
            'ap_gm'                      => $ap_gm,
            'date_ap_hd'                 => $date_ap_hd,
            'date_ap_gm'                 => $date_ap_gm,
            'ver_hr'                     => '0',
             'ap_koor'                    => $ap_koor,
            'ap_spv'                     => $ap_spv,
            'ap_pm'                     => $ap_pm,
            'ap_producer'               => $ap_producer,
            'ap_pipeline'               => $ap_pipeline,
            'date_ap_koor'               => $date_ap_koor,
            'date_ap_spv'                => $date_ap_spv,
            'date_ap_pm'                => $date_ap_pm,
            'date_ap_pipeline'          => $date_ap_pipeline,
            'email_koor'                => $email_koor,
            'email_pm'                  => $email_pm,
            'reason_leave'              => strtolower($request->input('reason')),
            'r_departure'               => $nama_provins,
            'r_after_leaving'           => $request->input('nama_city'),
            'plan_leave'              => $request->input('rencana'),
            'agreement'                 => $request->input('agree'),
            'ap_Infinite'               => $ap_infinite,
            'date_ap_Infinite'          => $date_ap_infinite,
            'resendmail'                => 2,
            ];
          
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return Redirect::route('leave/createEtc')
                    ->withErrors($validator)
                    ->withInput();
            } else {              
               if ($get_bacl_work_leaved != null) {
                   if ($get_end_leaved != null) {
                        // Leave::insert($data);                 
                        Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Data Leave Transaction']));
                        // $this->sendEmail3();
                        return Redirect::route('leave/transaction');
                   } else {
                      Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
                   }                
               } else {
                   Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
               } 
            }
    }

    public function storeAdvanceLeave(Request $request)
    {
        $init_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')   
        ->select([
            DB::raw('
            (
                select (
                    select initial_annual from users where id='.Auth::user()->id.' 
                ) - (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as remainannual')
        ])
        ->first();

        $taken = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')
        ->select([
            DB::raw('
            (
                (
                    select COALESCE(sum(total_day), 0) from leave_transaction where user_id='.Auth::user()->id.' and leave_category_id=1
                    and ap_hd  = 1
                    and ap_gm  = 1
                    and ver_hr = 1
                    and ap_hrd = 1
                )
            ) as leavetaken')
        ])
        ->first();

        $ent_annual = DB::table('users')
        ->leftJoin('initial_leave', 'initial_leave.user_id', '=', 'users.id')
        ->leftJoin('leave_category', 'leave_category.id', '=', 'initial_leave.leave_category_id')
        ->leftJoin('leave_transaction', 'leave_transaction.user_id', '=', 'users.id')    
        ->select([
            DB::raw('
            (
                (
                    select initial_annual from users where id='.Auth::user()->id.'
                ) 
            ) as entitle_ann')
        ])
        ->first();      
    
         $leave_d = DB::table('leave_transaction')
         ->whereRaw('Datediff(end_leave_date, leave_date)')
            ->first();
                  
                $email_koor = null;
                $email_pm = null;
                $ap_gm      = 0;
                $date_ap_gm = null;
                $ap_pipeline = 0;
                $date_ap_pipeline = null;
                $ap_spv = 0;
                $date_ap_spv = null;
                $ap_koor = 0;
                $date_ap_koor = null;
                $ap_pm = 0;
                $date_ap_pm = null;
                $ap_producer = 0;
                $date_producer  = null;
                $ap_hd = 0;
                $date_ap_hd = null;
                // ---------------------
                $ap_infinite = 0;
                $date_ap_infinite = null;

        // dept_category 1 = IT
        if (auth::user()->dept_category_id === 1) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_hr -> HRD -> GM (mike) 
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }              
        }
        // dept_category 2 = Finance
        elseif (auth::user()->dept_category_id === 2) {
               if (auth::user()->hd === 1) {
                // HOD -> Sow Kim -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // ----------------------------------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                 // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        // dept_category 3 = HRD
        elseif (auth::user()->dept_category_id === 3) {
               if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");               
             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept Category 4 = Operational ?
        elseif (auth::user()->dept_category_id === 4) {
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");                
                $email_pm = $request->input('sendtoPM'); 
        }
        //dept_category 5 = Facility
        elseif (auth::user()->dept_category_id === 5) {
               if (auth::user()->hd === 1) {
                // HOD -> John Reedel -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                // $ap_producer = 0;
                // $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                // Employee -> HOD -> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
             }
        }
        //dept category 6 = Production
        elseif (auth::user()->dept_category_id === 6) {
            if (auth::user()->hd === 1) {
                // HOD -> Choonmeng -> Ver_HR -> HRD -> GM (Mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d");
                // -----------------------
                $ap_infinite = 1;
                $date_ap_infinite = date('Y-m-d');
                $email_pm = $request->input('sendtoPM');

             } else {
                if (auth::user()->level_hrd != '0') {
                   if (auth::user()->level_hrd === 'Junior Pipeline') 
                   {        
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   } elseif (auth::user()->level_hrd === 'Junior Technical') 
                   {  
                    // Employee -> Senior Pipeline -> HOD -> Ver_HR -> HRD
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 0;
                            $date_ap_spv = null;
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                   }elseif (auth::user()->level_hrd === 'Senior Pipeline') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }elseif (auth::user()->level_hrd === 'Senior Technical') {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                   }
                   
                } else {
                        if (auth::user()->producer === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        } 
                        elseif (auth::user()->pm === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d");
                            $ap_pm = 1;
                            $date_ap_pm = date("Y-m-d");
                            $ap_producer = 1;
                            $date_producer  = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_pm = $request->input('sendtoPM');
                        }
                        elseif (auth::user()->koor === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_koor = 1;
                            $date_ap_koor = date("Y-m-d"); 
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d"); 
                            $email_pm = $request->input('sendtoPM');           
                        }
                        elseif (auth::user()->spv === 1) {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");  
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");  
                            $email_koor = $request->input('sendto');  
                            $email_pm = $request->input('sendtoPM');              
                        }
                        else {
                            $ap_pipeline = 0;
                            $date_ap_pipeline = date("Y-m-d");
                            $ap_spv = 1;
                            $date_ap_spv = date("Y-m-d");
                            $ap_gm = 1;
                            $date_ap_gm = date("Y-m-d");
                            $email_koor = $request->input('sendto');
                            $email_pm = $request->input('sendtoPM');   
                        }                   
                }
                
                
             }
        }
        // dept category 7 = Production LS
        elseif (auth::user()->dept_category_id === 7) {
            if (auth::user()->hd === 1) {
                // HOD -> Ver_HR -> HRD -> GM (mike)
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_hd = 1;
                $date_ap_hd = date("Y-m-d"); 

            } else {
                // Employee -> HOD-> Ver_HR -> HRD
                $ap_pipeline = 1;
                $date_ap_pipeline = date("Y-m-d");
                $ap_spv = 1;
                $date_ap_spv = date("Y-m-d");
                $ap_koor = 1;
                $date_ap_koor = date("Y-m-d");
                $ap_pm = 1;
                $date_ap_pm = date("Y-m-d");
                $ap_producer = 1;
                $date_producer  = date("Y-m-d");
                $ap_gm = 1;
                $date_ap_gm = date("Y-m-d");
                $email_pm = $request->input('sendtoPM');
            }
            
        }
        // dept category 8 = General
        elseif (auth::user()->dept_category_id === 8) {
               if (auth::user()->hd === 1) {
                    // HOD -> Ver_HR -> HRD 
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_hd = 1;
                    $date_ap_hd = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");
               } else {
                    // Employee -> HOD -> Ver_HR -> HRD
                    $ap_pipeline = 1;
                    $date_ap_pipeline = date("Y-m-d");
                    $ap_spv = 1;
                    $date_ap_spv = date("Y-m-d");
                    $ap_koor = 1;
                    $date_ap_koor = date("Y-m-d");
                    $ap_pm = 1;
                    $date_ap_pm = date("Y-m-d");
                    $ap_producer = 1;
                    $date_producer  = date("Y-m-d");
                    $ap_gm = 1;
                    $date_ap_gm = date("Y-m-d");     
                    $email_pm = $request->input('sendtoPM');              
               }
               
        }
        // dept category 9 = Management ?
        elseif (auth::user()->dept_category_id === 9) {
            $ap_pipeline = 1;
            $date_ap_pipeline = date("Y-m-d");
            $ap_spv = 1;
            $date_ap_spv = date("Y-m-d");
            $ap_koor = 1;
            $date_ap_koor = date("Y-m-d");
            $ap_pm = 1;
            $date_ap_pm = date("Y-m-d");
            $ap_producer = 1;
            $date_producer  = date("Y-m-d");
            $ap_hd = 1;
            $date_ap_hd = date("Y-m-d");
            $ap_gm = 1;
            $date_ap_gm = date("Y-m-d");
        }
        ////////////////////////////////////////////
            $tahun_libur = db::table('view_off_year')->whereDate('date','>=', $request->input('leave_date'))->whereDate('date','<=', $request->input('end_leave_date'))->get();
            $tahuan = array();           
            foreach ($tahun_libur as $key => $tahuan_value) {
                $tahuan[] = $tahuan_value->date;
            }

            $awal_cuti = strtotime($request->input('leave_date'));
            $akhir_cuti = strtotime($request->input('end_leave_date'));
            $haricuti = array();         
            for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                  
                        $haricuti[] = $i;
                   
                }
            }
            $jumlah_cuti = count($haricuti)-count($tahuan);

            $remaining = $request->input('entitlement')-$jumlah_cuti;
            //////////////////////////////////////////////////////

            /////////////////////////////////////////////////
            $start_leaved       = $request->input('leave_date');
            $end_leaved         = $request->input('end_leave_date');
            $back_work_leaved   = $request->input('back_work');

        if ($start_leaved <= $end_leaved) {
           $get_end_leaved = $end_leaved;
        } else {
            $get_end_leaved = null;
        }

        if ($back_work_leaved > $end_leaved) {
            $get_bacl_work_leaved = $back_work_leaved;
        } else {
            $get_bacl_work_leaved = null;
        }        
       
        if ($request->input('nama_provin') === '11') {
            $nama_provins = 'Aceh';
        }elseif ($request->input('nama_provin') === '12') {
            $nama_provins = 'Sumatera Utara';
        }elseif ($request->input('nama_provin') === '13') {
            $nama_provins = 'Sumatera Barat';
        }elseif ($request->input('nama_provin') === '14') {
            $nama_provins = 'Riau';
        }elseif ($request->input('nama_provin') === '15') {
            $nama_provins = 'Jambi';
        }elseif ($request->input('nama_provin') === '16') {
            $nama_provins = 'Sumatera Selatan';
        }elseif ($request->input('nama_provin') === '17') {
            $nama_provins = 'Bengkulu';
        }elseif ($request->input('nama_provin') === '18') {
            $nama_provins = 'Lampung';
        }elseif ($request->input('nama_provin') === '19') {
            $nama_provins = 'Kepulauan Bangka Belitung';
        }elseif ($request->input('nama_provin') === '21') {
            $nama_provins = 'Kepulauan Riau';
        }elseif ($request->input('nama_provin') === '31') {
            $nama_provins = 'Dki Jakarta';
        }elseif ($request->input('nama_provin') === '32') {
            $nama_provins = 'Jawa Barat';
        }elseif ($request->input('nama_provin') === '33') {
            $nama_provins = 'Jawa Tengah';
        }elseif ($request->input('nama_provin') === '34') {
            $nama_provins = 'Di Yogyakarta';
        }elseif ($request->input('nama_provin') === '35') {
            $nama_provins = 'Jawa Timur';
        }elseif ($request->input('nama_provin') === '36') {
            $nama_provins = 'Banten';
        }elseif ($request->input('nama_provin') === '51') {
            $nama_provins = 'Bali';
        }elseif ($request->input('nama_provin') === '52') {
            $nama_provins = 'Nusa Tenggara Barat';
        }elseif ($request->input('nama_provin') === '53') {
            $nama_provins = 'Nusa Tenggara Timur';
        }elseif ($request->input('nama_provin') === '61') {
            $nama_provins = 'Kalimantan Barat';
        }elseif ($request->input('nama_provin') === '62') {
            $nama_provins = 'Kalimantan Tengah';
        }elseif ($request->input('nama_provin') === '63') {
            $nama_provins = 'Kalimantan Selatan';
        }elseif ($request->input('nama_provin') === '64') {
            $nama_provins = 'Kalimantan Timur';
        }elseif ($request->input('nama_provin') === '65') {
            $nama_provins = 'Kalimantan Utara';
        }elseif ($request->input('nama_provin') === '71') {
            $nama_provins = 'Sulawesi Utara';
        }elseif ($request->input('nama_provin') === '72') {
            $nama_provins = 'Sulawesi Tengah';
        }elseif ($request->input('nama_provin') === '73') {
            $nama_provins = 'Sulawesi Selatan';
        }elseif ($request->input('nama_provin') === '74') {
            $nama_provins = 'Sulawesi Tenggara';
        }elseif ($request->input('nama_provin') === '75') {
            $nama_provins = 'Gorontalo';
        }elseif ($request->input('nama_provin') === '76') {
            $nama_provins = 'Sulawesi Barat';
        }elseif ($request->input('nama_provin') === '81') {
            $nama_provins = 'Maluku';
        }elseif ($request->input('nama_provin') === '82') {
            $nama_provins = 'Maluku Utara';
        }elseif ($request->input('nama_provin') === '91') {
            $nama_provins = 'Papua Barat';
        }elseif ($request->input('nama_provin') === '94') {
            $nama_provins = 'Papua';
        }else{
            $nama_provins = $request->input('nama_provin');
        }

         
        $rules = [
            'leave_date'        => 'required|date',
            'end_leave_date'    => 'required|date',
            'back_work'         => 'required|date',
            'reason'            => 'required|max:50'
           /* 'total_day'         => 'required|numeric',*/
           /* 'remain'            => 'required|numeric|min:0'*/
            ];
       
        $data = [
            'user_id'                    => Auth::user()->id,
            'leave_category_id'          => '1',
            'req_advance'                => 1,
            'request_by'                 => Auth::user()->first_name.' '.Auth::user()->last_name,
            'request_nik'                => Auth::user()->nik,
            'request_position'           => Auth::user()->position,
            'request_join_date'          => Auth::user()->join_date,
            'request_dept_category_name' => dept_category::where(['id' => Auth::user()->dept_category_id])->value('dept_category_name'),
            'period'                     => date('Y'),
            'leave_date'                 => $request->input('leave_date'),
            'end_leave_date'             => $get_end_leaved,
            'back_work'                  => $get_bacl_work_leaved,
            'total_day'                  => $jumlah_cuti,
            'taken'                      => $taken->leavetaken,
            'entitlement'                => $ent_annual->entitle_ann,
            'pending'                    => $init_annual->remainannual,
            'remain'                     => $remaining,
            'ap_hd'                      => $ap_hd,
            'ap_gm'                      => $ap_gm,
            'date_ap_hd'                 => $date_ap_hd,
            'date_ap_gm'                 => $date_ap_gm,
            'ver_hr'                     => '0',
            'ap_koor'                    => $ap_koor,
            'ap_spv'                     => $ap_spv,
            'ap_pm'                     => $ap_pm,
            'ap_producer'               => $ap_producer,
            'ap_pipeline'               => $ap_pipeline,
            'ap_Infinite'               => $ap_infinite,
            'date_ap_Infinite'          => $date_ap_infinite,
            'date_ap_koor'              => $date_ap_koor,
            'date_ap_spv'               => $date_ap_spv,
            'date_ap_pm'                => $date_ap_pm,
            'date_ap_pipeline'          => $date_ap_pipeline,
            'email_koor'                => $email_koor,
            'email_pm'                  => $email_pm,
            'reason_leave'              => strtolower($request->input('reason')),
            'r_departure'               => $nama_provins,
            'r_after_leaving'           => $request->input('nama_city'),
            'plan_leave'                => $request->input('rencana'),
            'agreement'                 => $request->input('agree'),
            'resendmail'                => 2,
            ];

            // dd($data);
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return Redirect::route('leave/create')
                    ->withErrors($validator)
                    ->withInput();
            } else {
               if ($get_bacl_work_leaved != null) {
                   if ($get_end_leaved != null) {
                        if ($remaining < 0) 
                        {
                         Session::flash('getError', Lang::get('messages.annual_balance_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                         return back();
                        } else {
                        // Leave::insert($data);                 
                        Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Data Leave Transaction']));
                        // $this->sendEmail3();
                        return Redirect::route('leave/transaction');
                        }
                   } else {
                      Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
                   }                
               } else {
                   Session::flash('getError', Lang::get('messages.annual_date_error', ['name' => auth::user()->first_name.' '.auth::user()->last_name]));
                     return back();
               }               
            }
    } 

    public function DetailStoreLeaveETC(Request $request)
    {
        $return   = "
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <h4 class='modal-title' id='showModalLabel'>Detail  </h4>
            </div>
            <div class='modal-body'>
                <div class='well'>
                    <
                </div>
            </div>
           
        ";
        return $return;
    }


//	Start Route Leave > Transaction
    public function indexLeaveTransaction()
    {
		if (Auth::user()->hd === 1 ) {                 
		return View::make('leave.indexTransactionHD');
			}
		
		if (Auth::user()->hr === 0 ) {
		return View::make('leave.indexTransactionUser');
			}		
		if (Auth::user()->hd === 0 ) {
		return View::make('leave.indexTransactionUser');
			}
		if (Auth::user()->gm === 0 ) {
		return View::make('leave.indexTransactionUser');
			}
		if (Auth::user()->hr === 1 ) {
		return View::make('leave.indexTransactionUser');
			}
	}

	public function indexLeaveTransactionUser()
    {
	return View::make('leave.indexTransactionUser');	
	}
	public function indexLeaveTransactionHD()
    {
	return View::make('leave.indexTransactionHD');	
	}

    public function getIndexTransactionHDFacilities()
    {
      $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_producer',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.ap_gm',
                'leave_transaction.resendmail'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id)
                ->get();

                return Datatables::of($select)
                ->edit_column('ap_producer', '@if ($ap_producer === 1){{"APPROVED"}} @elseif($ap_producer === 2){{"DISAPPROVED"}} @else {{"PENDING"}} @endif')

                ->edit_column('ver_hr', '@if ($ver_hr === 1){{ "VERIFIED" }} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif ($ap_producer === 0){{ "WAITING HEAD STUDIO" }} @elseif($ap_producer === 1){{"PENDING"}} @elseif ($ver_hr === 3){{"CANCEL"}} @else{{"--"}} @endif')

                ->edit_column('ap_hrd', '@if ($ap_hrd === 1){{ "APPROVED" }} @elseif ($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 0 and $ver_hr === 1){{"PENDING"}} @elseif($ver_hr === 0 && $ap_producer === 1){{"HR CHECKING"}} @elseif($ap_producer === 0){{"WAITING HEAD STUDIO"}}  @else {{"--"}}@endif')

                ->edit_column('ap_gm', '@if($ap_gm === 1){{"APPROVED"}} @elseif($ap_gm === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 1 and $ver_hr === 1 and $ap_producer === 1){{"PENDING"}} @elseif($ap_hrd === 0 and $ver_hr === 1 and $ap_producer === 1){{"WAITING HRD"}}  @elseif($ap_hrd === 0 and $ver_hr === 0 and $ap_producer === 1){{"HR CHECKING"}}  @elseif($ap_hrd === 0 and $ver_hr === 0 and $ap_producer === 0){{"WAITING HEAD STUDIO"}} @else {{"--"}} @endif')
           
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')

                ->add_column('actions',
                Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1 && $ver_hr === 1 )'
                      .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
                 ->make();       
    }

    public function getIndexLeaveTransactionHD()
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.ap_gm',
                'leave_transaction.resendmail'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id)
                ->get();

                return Datatables::of($select)
                ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @else {{"HR CHECKING"}} @endif')
                ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ver_hr === 1){{"PENDING"}} @elseif($ver_hr === 0){{"WAITING CHECKING"}} @else{{"--"}} @endif')
                ->edit_column('ap_gm', '@if($ap_gm === 1){{"APPROVED"}} @elseif($ap_gm === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 1 and $ver_hr === 1){{"PENDING"}} @elseif($ap_hrd === 0 and $ver_hr === 1){{"WAITING HR MANAGER"}} @elseif($ver_hr == 0){{"WAITING CHECKING"}}  @else {{"--"}} @endif')

                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')

                ->add_column('actions',
                    Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1 && $ver_hr === 1 )'
                      .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
                 ->make();
    }

    public function getIndexLeaveTransactionHDfa()
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_Infinite',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.ap_gm',
                'leave_transaction.resendmail'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id)
                ->get();

                return Datatables::of($select)
                ->edit_column('ap_Infinite', '@if($ap_Infinite === 0){{"APPROVED"}} @elseif($ap_Infinite === 2){{"DISAPPROVED"}} @else {{"PENDING"}} @endif')

                ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif($ver_hr === 0 and $ap_Infinite === 0){{"HR CHECKING"}} @elseif($ver_hr === 0 and $ap_Infinite === 1){{"WAITING INFINITE APPROVED"}} @else {{"--"}} @endif')

                ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 0 and $ver_hr === 1){{"PENDING"}} @elseif($ver_hr === 0 and $ap_Infinite === 1){{"HR CHECKING"}} @elseif($ap_Infinite === 0){{"WAITING INFINITE APPROVED"}} @else {{"--"}} @endif')  

                ->edit_column('ap_gm', '@if($ap_gm === 1){{"APPROVED"}} @elseif($ap_gm === 2){{"DISAPPROVED"}} @elseif($ap_gm === 0 and $ap_hrd === 1){{"PENDING"}} @elseif($ap_hrd === 0 and $ver_hr === 1){{"WAITING HR MANAGER"}} @elseif($ver_hr === 0 and $ap_Infinite === 1){{"HR CHECKING"}} @elseif($ap_Infinite === 0){{"WAITING INFINITE APPROVED"}} @else {{"--"}} @endif')

                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')

                ->add_column('actions',
                    Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1 && $ver_hr === 1 )'
                        .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
                ->make();
    }

    public function indexLeaveAllTransaction()
    {
        return View::make('leave.indexAllTransaction');
    }

     public function getIndexLeaveTransaction()
    {   
        // dept_category_id 6 = Production
        if (Auth::user()->dept_category_id != 6) {
            // dept_category_id 4 = Operation
            if (auth::user()->dept_category_id === 4) {
                 $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                //'leave_transaction.ap_gm'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_hrd', '@if ($ap_hrd === 1){{ "APPROVED" }} @elseif ($ap_hrd === 2){{"DISAPPROVED"}} @elseif ($ver_hr === 0){{ "WAITING HR" }} @elseif ($ver_hr === 1){{ "PENDING" }}@endif')
           
                ->edit_column('ver_hr', '@if ($ver_hr === 1){{ "VERIFIED" }} @elseif ($ver_hr === 0){{ "PENDING" }} @elseif ($ver_hr === 2){{ "UNVERIFIED" }} @elseif ($ver_hr === 3){{"CANCEL"}} @endif')
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
                ->add_column('actions',
                Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                '@if ( $ap_hrd === 1 && $ver_hr === 1 )'
                .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                .'@endif')
                 ->make(); 
            }

            if (auth::user()->hd === 1) {

                $GM = DB::table('users')
                ->select(db::raw('gm'))
                ->where('gm', '=', 1)
                ->value('gm'); 
                
                if ($GM != null) {
                $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.ap_gm'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_gm', '@if($ap_gm === 1){{"APPROVED"}} @elseif($ap_gm === 2){{"DISAPPROVED"}} @elseif($ver_hr === 0){{"WAITING VERIFY"}} @elseif($ver_hr === 1 and $ap_hrd === 0){{"WAITING HRD"}} @elseif($ap_hrd === 1){{"PENDING"}} @else {{"--"}} @endif')
               ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @else {{"PENDING"}} @endif')
               ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ver_hr === 0){{"WAITING VERIFY"}} @elseif($ver_hr === 1){{"PENDING"}} @else {{"--"}} @endif')
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
                ->add_column('actions',
                Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                '@if ( $ap_hrd === 1 && $ap_gm === 1 )'
                .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                .'@endif')
                ->make(); 
                }
                else {
                $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                //'leave_transaction.ap_gm'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_hrd', '@if ($ap_hrd === 1){{ "APPROVED" }} @elseif ($ap_hrd === 2){{"DISAPPROVED"}} @elseif ($ver_hr === 0){{ "WAITING HR" }} @elseif ($ver_hr === 1){{ "PENDING" }}@endif')
           
                ->edit_column('ver_hr', '@if ($ver_hr === 1){{ "VERIFIED" }} @elseif ($ver_hr === 0){{ "PENDING" }} @elseif ($ver_hr === 2){{ "UNVERIFIED" }} @elseif ($ver_hr === 3){{"CANCEL"}} @endif')
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
                ->add_column('actions',
                Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                '@if ( $ap_hrd === 1 && $ver_hr === 1 )'
                .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                .'@endif')
                ->make(); 
                } 

                      
            }else{
                /* officer */
               $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_hd',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.resendmail'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
               ->edit_column('ap_hd', '@if($ap_hd === 1){{"APPROVED"}} @elseif($ap_hd === 2){{"DISAPPROVED"}} @else {{"PENDING"}} @endif')
               ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif($ap_hd === 0 and $ver_hr === 0){{"WAITING HD"}} @elseif($ap_hd === 1 and $ver_hr === 0){{"PENDING"}} @else{{"--"}} @endif')             
               ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 5){{"--"}} @elseif($ver_hr === 1 and $ap_hrd === 0 and $ap_hd === 1){{"PENDING"}} @elseif($ver_hr === 0 and $ap_hd === 0 and $ap_hrd === 0){{"WAITING HD"}} @elseif($ver_hr === 0 and $ap_hrd === 0 and $ap_hd === 1){{"WAITING VERIFY"}} @else{{"--"}} @endif')
               ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')                
               ->add_column('actions',
                     Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file'])                    
                    .'@if ( $ap_hrd === 1 && $ap_hd === 1 )'
                    .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif'
                )
               ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
               ->make();       
            }
        } else {           
             $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_koor',
                'leave_transaction.ap_pm',
                'leave_transaction.ap_hd',                
                'leave_transaction.ap_hrd',
                
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_koor', '@if ($ap_koor === 1) {{"APPROVED"}} @elseif ($ap_koor === 2) {{"DISAPPROVED"}} @else {{"PENDING"}} @endif  ') 
                      
                ->edit_column('ap_pm', '@if ($ap_pm === 1) {{"APPROVED"}} @elseif ($ap_pm === 2) {{"DISAPPROVED"}} @elseif($ap_koor === 2){{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}}  @else {{"PENDING"}} @endif ')  

                ->edit_column('ap_hd', '@if ($ap_hd === 1) {{"APPROVED"}} @elseif ($ap_hd === 2) {{"DISAPPROVED"}} @elseif ($ap_koor === 2) {{"--"}} @elseif ($ap_pm === 2){{"--"}} @elseif ($ap_hrd === 2) {{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}} @elseif ($ap_pm === 0){{"WAITING PM"}} @else {{"PENDING"}} @endif')

                ->edit_column('ap_hrd', '@if ($ap_hrd === 1) {{"VERIFIED"}} @elseif ($ap_hrd === 2) {{"UNVERIFIED"}} @elseif ($ap_koor === 2) {{"--"}} @elseif ($ap_pm === 2){{"--"}} @elseif ($ap_hd === 2){{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}} @elseif ($ap_pm === 0){{"WAITING PM"}} @elseif ($ap_hd === 0){{"WAITING MANAGER"}}  @else {{"PENDING"}} @endif')

               
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
                ->add_column('actions',
                    Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1  )'
                    .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->make();              

        }		
    }

    public function getIndexTransactionOperation()
    {
         $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_hd',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.resendmail'
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
               ->edit_column('ap_hd', '@if($ap_hd === 1){{"APPROVED"}} @elseif($ap_hd === 2){{"DISAPPROVED"}} @else {{"PENDING"}} @endif')
               ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif($ap_hd === 0 and $ver_hr === 0){{"WAITING HD"}} @elseif($ap_hd === 1 and $ver_hr === 0){{"PENDING"}} @else{{"--"}} @endif')             
               ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 5){{"--"}} @elseif($ver_hr === 1 and $ap_hrd === 0 and $ap_hd === 1){{"PENDING"}} @elseif($ver_hr === 0 and $ap_hd === 0 and $ap_hrd === 0){{"WAITING HD"}} @elseif($ver_hr === 0 and $ap_hrd === 0 and $ap_hd === 1){{"WAITING VERIFY"}} @else{{"--"}} @endif')
               ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')                
               ->add_column('actions',
                     Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file'])                    
                    .'@if ( $ap_hrd === 1 && $ap_hd === 1 )'
                    .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif'
                )
               ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
               ->make(); 
    }

    public function getIndexTransactionProduction()
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_koor',
                'leave_transaction.ap_pm',
                'leave_transaction.ap_hd', 
                'leave_transaction.ver_hr',               
                'leave_transaction.ap_hrd',
                'leave_transaction.resendmail'
                
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_koor', '@if ($ap_koor === 1) {{"APPROVED"}} @elseif ($ap_koor === 2) {{"DISAPPROVED"}} @elseif($ap_koor === 0){{"PENDING"}} @else {{"--"}} @endif  ') 
                      
                ->edit_column('ap_pm', '@if ($ap_pm === 1) {{"APPROVED"}} @elseif ($ap_pm === 2) {{"DISAPPROVED"}} @elseif($ap_koor === 2){{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}}  @else {{"PENDING"}} @endif ')  

                ->edit_column('ap_hd', '@if ($ap_hd === 1) {{"APPROVED"}} @elseif ($ap_hd === 2) {{"DISAPPROVED"}} @elseif ($ap_koor === 2) {{"--"}} @elseif ($ap_pm === 2){{"--"}} @elseif ($ap_hrd === 2) {{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}} @elseif ($ap_pm === 0){{"WAITING PM"}} @else {{"PENDING"}} @endif')

                ->edit_column('ver_hr', '@if ($ver_hr === 1) {{"VERIFIED"}} @elseif ($ver_hr === 2) {{"UNVERIFIED"}} @elseif ($ap_koor === 2) {{"--"}} @elseif ($ap_pm === 2){{"--"}} @elseif ($ap_hd === 2){{"--"}} @elseif ($ap_koor === 0) {{"WAITING COORDINATOR"}} @elseif ($ap_pm === 0){{"WAITING PM"}} @elseif ($ap_hd === 0){{"WAITING MANAGER"}}  @else {{"PENDING"}} @endif')

                ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ap_hrd === 0 and $ver_hr === 1){{"PENDING"}} @elseif($ver_hr === 0 and $ap_hd === 1){{"WAITING VERIFY"}} @elseif($ap_hd === 0 and $ap_pm === 1){{"WAITING HD"}} @elseif($ap_pm === 0 and $ap_koor === 1){{"WAITING PM"}} @elseif($ap_koor === 0){{"WAITING COORDINATOR"}} @else {{"--"}} @endif')

               
                ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
                ->add_column('actions',
                    Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1  )'
                    .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->add_column('sendmails',
                    '@if($resendmail <= 2 and $resendmail > 0)'
                        .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                    .'@endif'
                            )
                ->make();
    }
    // tes pipeline
    public function indexLeaveTransactionPipiline()
    {
         $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
                'leave_transaction.id',
                'users.nik',            
                'leave_transaction.request_by',
                'leave_category.leave_category_name',
                'leave_transaction.leave_date',
                'leave_transaction.total_day',
                'leave_transaction.ap_spv',
                'leave_transaction.ap_hd',
                'leave_transaction.ver_hr',
                'leave_transaction.ap_hrd',
                'leave_transaction.resendmail'               
                 ])
                ->where('leave_transaction.user_id', '=', Auth::user()->id);

                return Datatables::of($select)
                ->edit_column('ap_spv', '@if($ap_spv === 1){{"APPROVED"}} @elseif($ap_spv === 2){{"DISAPPROVED"}} @elseif($ap_spv === 0){{"PENDING"}} @else {{"--"}} @endif')
                ->edit_column('ap_hd', '@if($ap_hd === 1){{"APPROVED"}} @elseif($ap_hd === 2){{"DISAPPROVED"}} @elseif($ap_spv === 1){{"PENDING"}} @elseif($ap_spv === 0){{"WAITING SPV"}} @else {{"--"}} @endif')
                ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif($ap_hd === 1 and $ap_spv === 1){{"PENDING"}} @elseif($ap_hd === 0 and $ap_spv === 1){{"WAITING HD"}} @elseif($ap_hd === 0 and $ap_spv === 0){{"WAITING SPV"}} @else {{"--"}} @endif')
                ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ver_hr === 1 and $ap_hd == 1 and $ap_spv === 1){{"PENDING"}} @elseif($ver_hr === 0 and $ap_hd == 1 and $ap_spv === 1){{"WAITING VERIFY"}} @elseif($ver_hr === 0 and $ap_hd == 0 and $ap_spv === 1){{"WAITING HD"}} @elseif($ver_hr === 0 and $ap_hd == 0 and $ap_spv === 0){{"WAITING SPV"}} @else {{"-"}} @endif')
                ->add_column('actions',
                    Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
                    '@if ( $ap_hrd === 1  )'
                    .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
                    .'@endif')
                ->add_column('sendmails', 
                                '@if($resendmail <= 2 and $resendmail > 0)'
                                .Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}'])
                                .'@endif'
                            )
                ->make();              
    }
	
	 

    public function getIndexAllLeaveTransaction()
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
            'leave_transaction.id',
            'users.nik',
            'leave_transaction.request_by',
            'leave_category.leave_category_name',
            'leave_transaction.leave_date',
            'leave_transaction.total_day',
            'leave_transaction.ver_hr',
            'leave_transaction.ap_hd',
            'leave_transaction.ap_gm'
        ]);

        return Datatables::of($select)
        ->edit_column('ap_hd', '@if ($ap_hd === 1){{ "APPROVED" }} @elseif ($ap_hd === 2){{"DISAPPROVED"}} @elseif ($ver_hr === 0){{ "WAITING HR" }} @elseif ($ver_hr === 1){{ "PENDING" }}@endif')
        ->edit_column('ap_gm', '@if ($ap_gm === 1){{ "APPROVED" }} @elseif ($ap_gm === 2){{"DISAPPROVED"}} @elseif ($ver_hr === 1 and $ap_hd === 0){{ "WAITING HD" }} @elseif ($ver_hr === 0 and $ap_hd === 0){{ "WAITING HD" }} @elseif ($ver_hr === 0 and $ap_hd === 1){{ "WAITING HR" }} @elseif ($ap_hd === 1){{ "PENDING" }}@endif')
        ->edit_column('ver_hr', '@if ($ver_hr === 1){{ "VERIFIED" }} @elseif ($ver_hr === 0){{ "PENDING" }} @elseif ($ver_hr === 2){{ "UNVERIFIED" }}@endif')
        ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
        ->add_column('actions',
           
            Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail2\', [$id]) }}', 'class' => 'file']).
            '@if ($ap_hd === 1 && $ap_gm === 1 && $ver_hr === 1)'
            .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
            .'@endif'

            )

        ->make();
    }

   

    public function detailLeave($id)
    {
    	$leave = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->find($id);
    	$return   = "
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <h4 class='modal-title' id='showModalLabel'>Detail</h4>
            </div>
            <div class='modal-body'>
                <div class='well'>
                    <h4><strong><u>Leave Transaction	</u></strong></h4>
                    <strong>Request by :</strong> $leave->first_name $leave->last_name<br>
                    <strong>Period :</strong> $leave->period <br>
                    <strong>Join Date :</strong> $leave->join_date <br>
                    <strong>NIK :</strong> $leave->nik <br>
                    <strong>Position :</strong> $leave->position <br>
                    <strong>Department :</strong> $leave->dept_category_name <br>
                    <strong>Contact Address :</strong> $leave->address <br>
                    <strong>Leave Category :</strong> $leave->leave_category_name <br>
                    <strong>Start Leave :</strong> $leave->leave_date <br>
                    <strong>End Leave :</strong> $leave->end_leave_date <br>
                    <strong>Back to Work:</strong> $leave->back_work <br>
                    <strong>Total Day :</strong> $leave->total_day <br>
                    <strong>Remain :</strong> $leave->remain <br>
                </div>
                <div class='well'>
                    <h5><u>Additional</u></h5>
                    <strong>Destination :</strong> $leave->r_departure - $leave->r_after_leaving <br>
                    <strong>Reason :</strong> $leave->reason_leave <br>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
            </div>
        ";

		return $return;
    }

    
    public function printLeave($id)
    {
    	$select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->joinInitialLeave()
        ->select([
    		'*'
    	])
        ->where('leave_transaction.user_id', '=', Auth::user()->id)
        ->where('leave_transaction.ap_hd', '=', 1)
        //->where('leave_transaction.ap_gm', '=', 1)
    	->where('leave_transaction.ver_hr', '=', 1)
        //->where('leave_transaction.ap_hrd', '=', 5)
        ->find($id);
    	view()->share('select', $select);

		$pdf = App::make('dompdf.wrapper');
		$pdf->loadview('leave.print');
		return $pdf->stream();
    }

    public function print1Leave($id)
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->joinInitialLeave()
        ->select([
            '*'
        ])
         ->where('leave_transaction.user_id', '=', Auth::user()->id)
        //->where('leave_transaction.ap_hd', '=', 1)
        ->where('leave_transaction.ap_hrd', '=', 1)
        ->where('leave_transaction.ver_hr', '=', 1)
        ->find($id);
        view()->share('select', $select);

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadview('leave.print');
        return $pdf->stream();
    }
  
    public function indexOrganitation()
    {    

        $avatar = asset("storage/app/prof_pict/no_avatar.jpg");
       
        return View::make('leave.indexOraganitation', ['no_avatar' => $avatar]); 

    }

     public function meeting()
    {     
        $project = Project_Category::where('project_name', '!=', 'Operasional')->orderBY('project_name', 'asc')->get();   
        return View::make('meeting.meeting', ['project' => $project]);
    }

    public function storemeeting(Request $request)
    {

       $rules = [
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'job' => 'required|string'           
            ];

        $data = [
            'users_id' => Auth::user()->id,
            'room' => $request->input("room"),
            'start_time' =>$request->input('date').' '.$request->input("start_time"),
            'end_time' =>$request->input('date').' '.$request->input("end_time"),
            'date' => $request->input('date'),
            'Project' => $request->input("job"),
            'request_by' => Auth::user()->first_name.' '.Auth::user()->last_name,
            'status' => 1,          

        ];
           
            $validator = validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return Redirect::route('meeting')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                
                Meeting::insert($data);
                Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Meeting Room Success'])); 
                return Redirect::route('meeting');
            }
      } 

    public function indexMeetingAudit()
    {
        $meeting = DB::Table("meeting")->get();

        return view::make('meeting.meeting_auditing', ['meet' => $meeting]);
    }

    public function getINdexMeetingAudit()
    {
        $meeting = DB::Table("meeting")->select([
            'id', 'Project', 'request_by', 'start_time', 'end_time', 'date', 'status'
        ])->get();

        return Datatables::of($meeting)
        ->edit_column('start_time', '{{date("H:i:s A", (strtotime($start_time)))}}')
        ->edit_column('end_time', '{{date("H:i:s A", (strtotime($end_time)))}}')
        ->edit_column('date', '{{date("M, d Y", (strtotime($date)))}}')
        ->edit_column('status', '@if($status === 1){{"Active"}} @else {{"Deactive"}} @endif')
       ->add_column('actions',
                Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'detailMeeting\', [$id]) }}', 'class' => 'file'])            
            )
        ->make();
    }

     public function detailMeeting($id)
    {
        $meeting = Meeting::find($id);
        $t = DB::table('meeting')->where('id', $id)->first();
       
        $return   = "
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
            </div>
          <form method='POST' action='".URL::route('postDetailMeeting', [$id])."', $t->id)}}'  enctype='multipart/data'>
                ".csrf_field()." 
      
                <div class='modal-body'>
                 <div class='form-group'>
                <label for='room'>Status Room</label>
                <select class='form-control' id='room' required='true' name='room'>
                     <option value='3'>Select Status</option>
                    <option value='1'>Active</option>
                    <option value='0'>Deactive</option>
                </select>
              </div>            
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        <button type='submit' class='btn btn-primary'>Save changes</button>      
      </div>
      </form>             
         
    
         
        ";

        return $return;
    }

    public function postDetailMeeting(request $request, $id)
    {
        $rules = [
            'room' => 'required'
        ];

        $data = [
            'status' => $request->input('room')
        ];

       /* dd($data);*/
         $validator = validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return Redirect::route('meeting/audit')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                
                Meeting::where('id', '=', $id)->update($data);
                Session::flash('message', Lang::get('messages.data_inserted', ['data' => 'Meeting Room Success'])); 
                return back();
            }
    }

    public function indexYourProjects()
    {
        return view::make('production.yourProjects');
    }

    public function getGUuide()
    {
        $contents = storage_path("app/pdf/Guide WIS.pdf");

        return response()->download($contents);
    }

    public function guided()
    {
        return view::make('guide.index');
    }

    public function reSendMailLeave($id)
    {
        $email      = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->find($id);

        $hd = User::where('dept_category_id', auth::user()->dept_category_id)->where('hd', 1)->where('active', 1)->first();

        $ver_hr = User::where('hr', 1)->first();        

        $data = $email->resendmail - 1;
       
        if ($email->ap_koor === 0) { 
            if ($email->dept_category_id === 6){
                Mail::send('email.Reminders.pendingLeaveProduction', ['email' => $email], function($message) use ($email)
                    {
                        $message->to($email->email_koor, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                Mail::send('email.Reminders.pendingLeaveOfficer', ['email' => $email], function($message) use ($email)
                    {
                        $message->to($email->email_koor, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });                
            }         
        }elseif ($email->ap_pm === 0) {           
           if ($email->dept_category_id === 6){
                Mail::send('email.Reminders.pendingLeaveProduction', ['email' => $email], function($message) use ($email)
                    {
                        $message->to($email->email_pm, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                Mail::send('email.Reminders.pendingLeaveOfficer', ['email' => $email], function($message) use ($email)
                    {
                        $message->to($email->email_pm, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });                
            }   
        }elseif ($email->ap_hd === 0) {           
            if ($email->dept_category_id === 6){
                Mail::send('email.Reminders.pendingLeaveProduction', ['email' => $email], function($message) use ($email, $hd)
                    {
                        $message->to($hd->email, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                Mail::send('email.Reminders.pendingLeaveOfficer', ['email' => $email], function($message) use ($email, $hd)
                    {
                        $message->to($hd->email, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });                
            }   
        }elseif ($email->ver_hr === 0) {            
           if ($email->dept_category_id === 6){
                Mail::send('email.verMail', ['email' => $email], function($message) use ($email, $ver_hr)
                    {
                        $message->to($ver_hr->email, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                Mail::send('email.verMail', ['email' => $email], function($message) use ($email, $ver_hr)
                    {
                        $message->to($ver_hr->email, 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });                
            }   
        }elseif ($email->ap_hrd === 0) {            
            if ($email->dept_category_id === 6){
                Mail::send('email.Reminders.pendingLeaveProduction', ['email' => $email], function($message) use ($email)
                    {
                        $message->to('wahyuni.hasan@frameworks-studios.com', 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });
            } else {
                Mail::send('email.Reminders.pendingLeaveOfficer', ['email' => $email], function($message) use ($email)
                    {
                        $message->to('wahyuni.hasan@frameworks-studios.com', 'WIS')->subject('[Approval] Leave Application - '.$email->request_by.'');
                        $message->from('wis_system@frameworks-studios.com', 'WIS');
                    });                
            }   
        }
        Leave::where('id', $id)->update([
            'resendmail' => $data
        ]);
        Session::flash('message', Lang::get('messages.data_updated', ['data' => 'leave']));
        return redirect()->route('leave/transaction');
    }

    public function getIndexLeaveTransactionHRD()
    {
        $select = Leave::joinUsers()->joinDeptCategory()->joinLeaveCategory()->select([
            'leave_transaction.id',
            'users.nik',
            'leave_transaction.request_by',
            'leave_category.leave_category_name',
            'leave_transaction.leave_date',
            'leave_transaction.total_day',
            'leave_transaction.ver_hr',
            'leave_transaction.ap_hrd',
            'leave_transaction.resendmail'
        ])
        ->where('leave_transaction.user_id', auth::user()->id)
        ->get();

        return Datatables::of($select)
        ->edit_column('ver_hr', '@if($ver_hr === 1){{"VERIFIED"}} @elseif($ver_hr === 2){{"UNVERIFIED"}} @elseif($ver_hr === 0){{"PENDING"}} @else {{"--"}} @endif')
        ->edit_column('ap_hrd', '@if($ap_hrd === 1){{"APPROVED"}} @elseif($ap_hrd === 2){{"DISAPPROVED"}} @elseif($ver_hr === 1){{"PENDING"}} @elseif($ver_hr === 0){{"WAITING VERIFY"}} @else {{"--"}} @endif')
        ->edit_column('leave_date', '{!! date("M d, Y", strtotime($leave_date)) !!}')
        ->add_column('actions',           
            Lang::get('messages.btn_success', ['title' => 'Detail', 'url' => '{{ URL::route(\'leave/detail\', [$id]) }}', 'class' => 'file']).
            '@if ($ap_hrd === 1 && $ver_hr === 1)'
            .Lang::get('messages.btn_print', ['title' => 'Print', 'url' => '{{ URL::route(\'leave/print1\', [$id]) }}', 'class' => 'print'])
            .'@endif')
        ->add_column('sendmails',
            '@if($resendmail <= 2 and $resendmail > 0)'.Lang::get('messages.btn_resendmail', ['title' => 'send back email notification x{{ $resendmail }}', 'url' => '{{ URL::route(\'leave/reSendMailLeave\', [$id]) }}']).'@endif')
        ->make();
    }

   
}